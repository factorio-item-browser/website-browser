import { ItemType, ModType, RecipeType } from "../src/constants/entity";
import { Item } from "../src/model/entity/item.ts";
import { RecipeWithTechnology } from "../src/model/entity/recipe.ts";
import { Mod } from "../src/model/entity/mod.ts";
import { Machine } from "../src/model/entity/machine.ts";

export const exampleItems = {
    copperCable: {
        key: "item.copper-cable",
        type: ItemType.Item,
        name: "copper-cable",
        label: "Copper cable",
        description: "Can also be used to manually connect and disconnect electric poles and power switches with [Build].",
        iconHash: "item-copper-cable",
    } as Item,
    crudeOil: {
        key: "fluid.crude-oil",
        type: ItemType.Fluid,
        name: "crude-oil",
        label: "Crude oil",
        description: "",
        iconHash: "fluid-crude-oil",
    } as Item,
    electronicCircuit: {
        key: "item.electronic-circuit",
        type: ItemType.Item,
        name: "electronic-circuit",
        label: "Electronic circuit",
        description: "",
        iconHash: "item-electronic-circuit",
    } as Item,
    ironOre: {
        key: "resource.iron-ore",
        type: ItemType.Resource,
        name: "iron-ore",
        label: "Iron ore",
        description: "",
        iconHash: "resource-iron-ore",
    } as Item,
};

export const exampleMods = {
    base: {
        key: "mod.base",
        type: ModType.Mod,
        name: "base",
        label: "Base mod",
        description: "",
        author: "Factorio team",
        version: "1.2.3",
        iconHash: "mod-base",
    } as Mod,
    spaceExploration: {
        key: "mod.space-exploration",
        type: ModType.Mod,
        name: "space-exploration",
        label: "Space Exploration",
        description: "",
        author: "Earendel",
        version: "0.6.9",
        iconHash: "mod-space-exploration",
    } as Mod,
}

export const exampleMachines = {
    assemblingMachine3: {
        "key": "machine.assembling-machine-3",
        "type": "machine",
        "name": "assembling-machine-3",
        "label": "Montagemaschine 3",
        "description": "",
        "iconHash": "machine-assembling-machine-3",
        "craftingCategories": [
            "basic-crafting",
            "crafting",
            "advanced-crafting",
            "crafting-with-fluid"
        ],
        "resourceCategories": [],
        "speed": 1.25,
        "itemSlots": 255,
        "fluidInputSlots": 1,
        "fluidOutputSlots": 1,
        "moduleSlots": 4,
        "energyUsage": 375,
        "energyUsageUnit": "kW"
    } as Machine,
    character: {
        "key": "machine.character",
        "type": "machine",
        "name": "character",
        "label": "Charakter",
        "description": "",
        "iconHash": "machine-character",
        "craftingCategories": ["crafting"],
        "resourceCategories": ["basic-solid"],
        "speed": 0.5,
        "itemSlots": 255,
        "fluidInputSlots": 0,
        "fluidOutputSlots": 0,
        "moduleSlots": 0,
        "energyUsage": 0,
        "energyUsageUnit": ""
    } as Machine,
    pumpjack: {
        "key": "machine.pumpjack",
        "type": "machine",
        "name": "pumpjack",
        "label": "Pumpjack",
        "description": "",
        "iconHash": "machine-pumpjack",
        "craftingCategories": [],
        "resourceCategories": ["basic-fluid"],
        "speed": 1,
        "itemSlots": 255,
        "fluidInputSlots": 0,
        "fluidOutputSlots": 1,
        "moduleSlots": 2,
        "energyUsage": 90,
        "energyUsageUnit": "kW"
    } as Machine,
};

export const exampleRecipes = {
    advancedCircuit: {
        key: "crafting.advanced-circuit",
        type: "crafting",
        name: "advanced-circuit",
        label: "Advanced circuit",
        description: "",
        iconHash: "item-advanced-circuit",
        category: "crafting",
        time: 6,
        ingredients: [
            {
                "key": "item.plastic-bar",
                "type": "item",
                "iconHash": "item-plastic-bar",
                "amount": 2
            },
            {
                "key": "item.copper-cable",
                "type": "item",
                "iconHash": "item-copper-cable",
                "amount": 4
            },
            {
                "key": "item.electronic-circuit",
                "type": "item",
                "iconHash": "item-electronic-circuit",
                "amount": 2
            }
        ],
        products: [
            {
                "key": "item.advanced-circuit",
                "type": "item",
                "iconHash": "item-advanced-circuit",
                "amount": 1
            }
        ],
        technology: {
            "key": "technology.advanced-electronics",
            "type": "technology",
            "name": "advanced-electronics",
            "iconHash": "technology-advanced-electronics",
        },
    } as RecipeWithTechnology,
    copperCable: {
        key: "recipe.copper-cable",
        type: RecipeType.Crafting,
        name: "copper-cable",
        label: "Copper cable",
        description:
            "Can also be used to manually connect and disconnect electric poles and power switches with [Build].",
        iconHash: "item-copper-cable",
        category: "crafting",
        ingredients: [
            {
                key: "item.copper-plate",
                type: ItemType.Item,
                name: "copper-plate",
                label: "Copper plate",
                description: "",
                iconHash: "item-copper-plate",
                amount: 1,
            },
        ],
        products: [
            {
                key: "item.copper-cable",
                type: ItemType.Item,
                name: "copper-cable",
                label: "Copper cable",
                description: "",
                iconHash: "item-copper-cable",
                amount: 2,
            },
        ],
        time: 0.5,
        technology: null,
    } as RecipeWithTechnology,
    crudeOil: {
        key: "mining.crude-oil",
        type: RecipeType.Mining,
        name: "crude-oil",
        label: "Crude oil",
        description: "",
        iconHash: "item-crude-oil",
        category: "",
        ingredients: [
            {
                key: "resource.crude-oil",
                type: ItemType.Resource,
                name: "crude-oil",
                label: "Crude oil",
                description: "",
                iconHash: "resource-crude-oil",
                amount: 1,
            },
        ],
        products: [
            {
                key: "fluid.crude-oil",
                type: ItemType.Fluid,
                name: "crude-oil",
                label: "Crude oil",
                description: "",
                iconHash: "fluid-crude-oil",
                amount: 10,
            },
        ],
        time: 0.5,
        technology: null,
    } as RecipeWithTechnology,
    electronicCircuit: {
        key: "crafting.electronic-circuit",
        type: RecipeType.Crafting,
        name: "electronic-circuit",
        label: "Electronic circuit",
        description: "",
        iconHash: "item-electronic-circuit",
        category: "crafting",
        time: 0.5,
        ingredients: [
            {
                key: "item.iron-plate",
                type: ItemType.Item,
                name: "iron-plate",
                label: "Iron plate",
                description: "",
                iconHash: "item-iron-plate",
                amount: 1,
            },
            {
                key: "item.copper-cable",
                type: ItemType.Item,
                name: "copper-cable",
                label: "Copper cable",
                description: "",
                iconHash: "item-copper-cable",
                amount: 3,
            },
        ],
        products: [
            {
                key: "item.electronic-circuit",
                type: ItemType.Item,
                name: "electronic-circuit",
                label: "Electronic circuit",
                description: "",
                iconHash: "item-electronic-circuit",
                amount: 1,
            },
        ],
        technology: null,
    } as RecipeWithTechnology,
    satellite: {
        key: "rocket-launch.satellite",
        type: RecipeType.RocketLaunch,
        name: "satellite",
        label: "Satellite",
        description: "The satellite should be put into the rocket.",
        iconHash: "item-satellite",
        category: "",
        time: 0,
        ingredients: [
            {
                key: "item.satellite",
                type: ItemType.Item,
                name: "satellite",
                label: "Satellite",
                description: "The satellite should be put into the rocket.",
                iconHash: "item-satellite",
                amount: 1,
            },
        ],
        products: [
            {
                key: "item.space-science-pack",
                type: ItemType.Item,
                name: "space-science-pack",
                label: "Space science pack",
                description: "",
                iconHash: "item-space-science-pack",
                amount: 1000,
            },
        ],
        technology: null,
    } as RecipeWithTechnology,
};

import { mdiChevronRight, mdiPickaxe, mdiRocketLaunch } from "@mdi/js";
import classNames from "classnames";
import { RecipeType } from "../../constants/entity.ts";
import { Recipe } from "../../model/entity/recipe.ts";
import { RecipeIngredientsCard } from "../card/RecipeIngredientsCard.tsx";
import { RecipeProductsCard } from "../card/RecipeProductsCard.tsx";
import { Icon } from "../icon/Icon.tsx";
import styles from "./RecipeDetails.module.css";

type Props = {
    /** The recipe to display the details for. */
    recipe: Recipe;
    /** The additional className to attach. */
    className?: string;
};

const iconPaths = {
    [RecipeType.Crafting]: mdiChevronRight,
    [RecipeType.RocketLaunch]: mdiRocketLaunch,
    [RecipeType.Mining]: mdiPickaxe,
};

export const RecipeDetails = ({ recipe, className }: Props) => {
    const classes = classNames(className, styles.details);
    const separatorClasses = classNames(styles.separator, {
        [styles.crafting]: recipe.type === RecipeType.Crafting,
    });

    return (
        <div className={classes}>
            <RecipeIngredientsCard className={styles.ingredients} recipe={recipe} />
            <Icon className={separatorClasses} path={iconPaths[recipe.type]} />
            <RecipeProductsCard className={styles.products} recipe={recipe} />
        </div>
    );
};

import type { Meta, StoryObj } from "@storybook/react";
import { exampleRecipes } from "../../../.storybook/examples.ts";
import { RecipeDetails } from "./RecipeDetails.tsx";

const meta: Meta<typeof RecipeDetails> = {
    title: "recipe/RecipeDetails",
    component: RecipeDetails,
};
export default meta;

type Story = StoryObj<typeof RecipeDetails>;

export const Component: Story = {
    args: {
        recipe: exampleRecipes.electronicCircuit,
    },
};

export const Mining: Story = {
    name: "Mining recipe",
    args: {
        recipe: exampleRecipes.crudeOil,
    },
};

export const RocketLaunch: Story = {
    name: "Rocket launch recipe",
    args: {
        recipe: exampleRecipes.satellite,
    },
};

import classNames from "classnames";
import { CSSProperties, useRef } from "react";
import { useOverflowWidth } from "../../hooks/overflow.ts";
import { Recipe } from "../../model/entity/recipe.ts";
import styles from "./CompactRecipe.module.css";
import { RecipeItem } from "./part/RecipeItem.tsx";
import { RecipeSeparator } from "./part/RecipeSeparator.tsx";

type Props = {
    /** The recipe to display. */
    recipe: Recipe;
    /** The additional className to attach. */
    className?: string;
};

export const CompactRecipe = ({ recipe, className }: Props) => {
    const ref = useRef<HTMLDivElement>(null);
    const width = useOverflowWidth(ref);

    const classes = classNames(className, styles.container, {
        [styles.animated]: width > 0,
    });
    const classesRecipe = classNames(styles.recipe, {
        [styles.animated]: width > 0,
    });

    let style: CSSProperties = {};
    if (width > 0) {
        style = {
            ["--recipe-width"]: `${width}px`,
        } as CSSProperties;
    }

    return (
        <div className={classes}>
            <div className={classesRecipe} ref={ref} style={style}>
                {recipe.ingredients.map((ingredient) => (
                    <RecipeItem className={styles.icon} key={ingredient.key} item={ingredient} />
                ))}
                <RecipeSeparator className={styles.icon} recipe={recipe} />
                {recipe.products.map((product) => (
                    <RecipeItem className={styles.icon} key={product.key} item={product} />
                ))}
            </div>
        </div>
    );
};

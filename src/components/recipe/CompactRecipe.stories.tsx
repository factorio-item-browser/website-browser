import type { Meta, StoryObj } from "@storybook/react";
import { exampleItems, exampleRecipes } from "../../../.storybook/examples.ts";
import { CompactRecipe } from "./CompactRecipe.tsx";

const meta: Meta<typeof CompactRecipe> = {
    title: "recipe/CompactRecipe",
    component: CompactRecipe,
    parameters: {
        backgrounds: {
            default: "none",
        },
    },
};
export default meta;

type Story = StoryObj<typeof CompactRecipe>;

export const Component: Story = {
    args: {
        recipe: exampleRecipes.electronicCircuit,
    },
};

export const Overflow: Story = {
    args: {
        recipe: {
            ...exampleRecipes.electronicCircuit,
            products: [
                { ...exampleItems.electronicCircuit, amount: 1 },
                { ...exampleItems.electronicCircuit, amount: 2 },
                { ...exampleItems.electronicCircuit, amount: 3 },
                { ...exampleItems.electronicCircuit, amount: 4 },
                { ...exampleItems.electronicCircuit, amount: 5 },
                { ...exampleItems.electronicCircuit, amount: 6 },
            ],
        },
    },
};

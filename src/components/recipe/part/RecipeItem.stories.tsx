import type { Meta, StoryObj } from "@storybook/react";
import { exampleItems } from "../../../../.storybook/examples.ts";
import { ItemWithAmount } from "../../../model/entity/item.ts";
import { RecipeItem } from "./RecipeItem.tsx";

const meta: Meta<typeof RecipeItem> = {
    title: "recipe/part/RecipeItem",
    component: RecipeItem,
    parameters: {
        backgrounds: {
            default: "none",
        },
    },
};
export default meta;

type Story = StoryObj<typeof RecipeItem>;

function createItem(amount: number): ItemWithAmount {
    return {
        ...exampleItems.electronicCircuit,
        amount: amount,
    };
}

export const Component: Story = {
    args: {
        item: createItem(42),
    },
};

export const AmountFormatting: Story = {
    name: "Amount formatting",
    render: () => {
        return (
            <div
                style={{
                    display: "grid",
                    gridTemplateColumns: "repeat(4, min-content)",
                    gridGap: "1rem",
                    alignItems: "center",
                    textAlign: "center",
                }}
            >
                <RecipeItem item={createItem(42)} />
                <RecipeItem item={createItem(12345)} />
                <RecipeItem item={createItem(12345678)} />
                <RecipeItem item={createItem(0.42)} />
            </div>
        );
    },
};

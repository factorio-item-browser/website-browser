import classNames from "classnames";
import { formatAmount } from "../../../format/recipe.ts";
import { ItemWithAmount } from "../../../model/entity/item.ts";
import { IconWithText } from "../../icon/IconWithText.tsx";
import { EntityLink } from "../../link/EntityLink.tsx";
import styles from "./RecipeItem.module.css";

type Props = {
    /** The recipe to display. */
    item: ItemWithAmount;
    /** The additional className to attach. */
    className?: string;
};

export const RecipeItem = ({ item, className }: Props) => {
    if (item.amount <= 0) {
        return null;
    }

    const classes = classNames(className, styles.link);
    const formattedAmount = formatAmount(item.amount);

    return (
        <EntityLink className={classes} entity={item} box>
            <IconWithText hash={item.iconHash} text={formattedAmount} top left />
        </EntityLink>
    );
};

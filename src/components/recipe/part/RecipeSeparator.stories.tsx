import type { Meta, StoryObj } from "@storybook/react";
import { RecipeType } from "../../../constants/entity.ts";
import { TextStyle } from "../../../constants/text.ts";
import { Recipe } from "../../../model/entity/recipe.ts";
import { Text } from "../../text/Text.tsx";
import { RecipeSeparator } from "./RecipeSeparator.tsx";

const meta: Meta<typeof RecipeSeparator> = {
    title: "recipe/part/RecipeSeparator",
    component: RecipeSeparator,
};
export default meta;

type Story = StoryObj<typeof RecipeSeparator>;

export const Component: Story = {
    args: {
        recipe: {
            type: RecipeType.Crafting,
            time: 42,
        } as Recipe,
    },
};

export const RecipeTypes: Story = {
    name: "Recipe types",
    render: () => (
        <div
            style={{
                display: "grid",
                gridTemplateColumns: "repeat(2, min-content)",
                gridGap: "1rem",
                alignItems: "center",
                textAlign: "left",
            }}
        >
            <Text text={"RecipeType.Crafting:"} style={TextStyle.Bold} />
            <RecipeSeparator recipe={{ type: RecipeType.Crafting, time: 42 } as Recipe} />
            <Text text={"RecipeType.Mining:"} style={TextStyle.Bold} />
            <RecipeSeparator recipe={{ type: RecipeType.Mining, time: 42 } as Recipe} />
            <Text text={"RecipeType.RocketLaunch:"} style={TextStyle.Bold} />
            <RecipeSeparator recipe={{ type: RecipeType.RocketLaunch, time: 42 } as Recipe} />
        </div>
    ),
};

export const TimeFormatting: Story = {
    name: "Time formatting",
    render: () => (
        <div
            style={{
                display: "grid",
                gridTemplateColumns: "repeat(4, min-content)",
                gridGap: "2rem",
            }}
        >
            <RecipeSeparator recipe={{ type: RecipeType.Crafting, time: 420 } as Recipe} />
            <RecipeSeparator recipe={{ type: RecipeType.Crafting, time: 2520 } as Recipe} />
            <RecipeSeparator recipe={{ type: RecipeType.Crafting, time: 15120 } as Recipe} />
            <RecipeSeparator recipe={{ type: RecipeType.Crafting, time: 420000 } as Recipe} />
        </div>
    ),
};

import { mdiChevronRight, mdiPickaxe, mdiRocketLaunch } from "@mdi/js";
import classNames from "classnames";
import { RecipeType } from "../../../constants/entity.ts";
import { formatCraftingTime } from "../../../format/recipe.ts";
import { Recipe } from "../../../model/entity/recipe.ts";
import { IconWithText } from "../../icon/IconWithText.tsx";
import styles from "./RecipeSeparator.module.css";

type Props = {
    /** The recipe to display the separator for. */
    recipe: Recipe;
    /** The additional className to attach. */
    className?: string;
};

const iconPaths = {
    [RecipeType.Crafting]: mdiChevronRight,
    [RecipeType.RocketLaunch]: mdiRocketLaunch,
    [RecipeType.Mining]: mdiPickaxe,
};

export const RecipeSeparator = ({ recipe, className }: Props) => {
    const iconClasses = classNames({
        [styles.crafting]: recipe.type === RecipeType.Crafting,
    });
    const textClasses = classNames({
        [styles.infinite]: recipe.time > 86400,
    });

    let label = "";
    if (recipe.type === RecipeType.Crafting && recipe.time > 0) {
        label = formatCraftingTime(recipe.time);
    }

    return (
        <IconWithText
            className={className}
            iconClassName={iconClasses}
            textClassName={textClasses}
            path={iconPaths[recipe.type]}
            text={label}
            bottom
        />
    );
};

import classNames from "classnames";
import { useState } from "react";
import styles from "./Header.module.css";
import { Logo } from "./part/Logo.tsx";
import { SearchInput } from "./part/SearchInput.tsx";

type Props = {
    /** Whether to use the big variant of the header. */
    big?: boolean;
    /** The additional className to attach. */
    className?: string;
};

export const Header = ({ big, className }: Props) => {
    const [query, setQuery] = useState("");

    const classes = classNames(className, styles.header, {
        [styles.big]: big,
    });

    return (
        <header className={classes}>
            <Logo className={styles.logo} big={big} />
            <SearchInput className={styles.search} value={query} onChange={setQuery} center={big} />
        </header>
    );
};

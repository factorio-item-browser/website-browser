import type { Meta, StoryObj } from "@storybook/react";
import { Logo } from "./Logo.tsx";

const meta: Meta<typeof Logo> = {
    title: "layout/part/Logo",
    component: Logo,
    parameters: {
        backgrounds: {
            default: "water",
        },
    },
};
export default meta;

type Story = StoryObj<typeof Logo>;

export const Component: Story = {
    args: {
        big: false,
    },
};

export const Big: Story = {
    name: "Big variant",
    args: {
        big: true,
    },
};

import { useArgs } from "@storybook/preview-api";
import type { Meta, StoryObj } from "@storybook/react";
import { SearchInput } from "./SearchInput.tsx";

const meta: Meta<typeof SearchInput> = {
    title: "layout/part/SearchInput",
    component: SearchInput,
    parameters: {
        backgrounds: {
            default: "water",
        },
    },
};
export default meta;

type Story = StoryObj<typeof SearchInput>;

export const Component: Story = {
    args: {
        value: "",
        closable: false,
    },
    render: (args) => {
        const [, setArgs] = useArgs();
        const handleChange = (value: string) => {
            args.onChange?.(value);
            setArgs({ value });
        };

        return <SearchInput {...args} onChange={handleChange} />;
    },
};

export const Closable: Story = {
    name: "Use case: With close icon",
    args: {
        value: "",
        closable: true,
    },
    render: (args) => {
        const [, setArgs] = useArgs();
        const handleChange = (value: string) => {
            args.onChange?.(value);
            setArgs({ value });
        };

        return <SearchInput {...args} onChange={handleChange} />;
    },
};

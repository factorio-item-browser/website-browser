import type { Meta, StoryObj } from "@storybook/react";
import { Copyright } from "./Copyright.tsx";

const meta: Meta<typeof Copyright> = {
    title: "layout/part/Copyright",
    component: Copyright,
    parameters: {
        backgrounds: {
            default: "water",
        },
    },
};
export default meta;

type Story = StoryObj<typeof Copyright>;

export const Component: Story = {};

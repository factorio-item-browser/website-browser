import classNames from "classnames";
import logo from "../../../assets/logo.webp";
import { InternalLink } from "../../link/InternalLink.tsx";
import styles from "./Logo.module.css";

type Props = {
    /** Whether to use the big variant of the logo. */
    big?: boolean;
    /** The additional className to attach. */
    className?: string;
};

export const Logo = ({ big = false, className }: Props) => {
    const classes = classNames(className, styles.wrapper, {
        [styles.big]: big,
    });

    return (
        <InternalLink className={classes} route={""} box>
            <img className={styles.logo} src={logo} alt="Factorio Item Browser" />
        </InternalLink>
    );
};

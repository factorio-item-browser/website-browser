import { mdiClose, mdiMagnify } from "@mdi/js";
import classNames from "classnames";
import { FormEvent, MouseEvent } from "react";
import { useTranslation } from "react-i18next";
import { Icon } from "../../icon/Icon.tsx";
import styles from "./SearchInput.module.css";

type Props = {
    /** The current value of the search input. */
    value: string;
    /** Whether to center the search input, limiting its width. */
    center?: boolean;
    /** The handler for when the value of the search input has changed. */
    onChange: (value: string) => void;
    /** Whether the search input can be closed with an X icon. */
    closable?: boolean;
    /** The handler for when the search input gets closed. */
    onClose?: () => void;
    /** The additional className to attach. */
    className?: string;
};

export const SearchInput = ({ value, center, onChange, closable = false, onClose, className }: Props) => {
    const { t } = useTranslation("components");

    const handleChange = (event: FormEvent<HTMLInputElement>) => {
        if (onChange) {
            onChange(event.currentTarget.value);
        }
    };
    const handleCloseClick = (event: MouseEvent<HTMLDivElement>) => {
        event.preventDefault();
        event.stopPropagation();
        if (onClose) {
            onClose();
        }
    };
    const handleSubmit = (event: FormEvent<HTMLFormElement>) => {
        event.preventDefault();
        event.stopPropagation();
    };

    let closeIcon = null;
    if (closable) {
        closeIcon = (
            <div className={styles.close} onClick={handleCloseClick}>
                <Icon className={styles.closeIcon} path={mdiClose} />
            </div>
        );
    }

    const classes = classNames(className, styles.search, {
        [styles.center]: center,
    });

    return (
        <form className={classes} onSubmit={handleSubmit}>
            <Icon className={styles.searchIcon} path={mdiMagnify} />
            <input
                type={"search"}
                name={"query"}
                className={styles.input}
                autoComplete={"off"}
                placeholder={t("components:layout.search-input.placeholder")}
                value={value}
                onChange={handleChange}
            />
            {closeIcon}
        </form>
    );
};

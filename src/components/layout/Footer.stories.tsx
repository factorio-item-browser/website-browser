import type { Meta, StoryObj } from "@storybook/react";
import { Footer } from "./Footer.tsx";

const meta: Meta<typeof Footer> = {
    title: "layout/Footer",
    component: Footer,
    parameters: {
        backgrounds: {
            default: "none",
        },
    },
};
export default meta;

type Story = StoryObj<typeof Footer>;

export const Component: Story = {};

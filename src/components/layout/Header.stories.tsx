import type { Meta, StoryObj } from "@storybook/react";
import { Header } from "./Header.tsx";

const meta: Meta<typeof Header> = {
    title: "layout/Header",
    component: Header,
    parameters: {
        backgrounds: {
            default: "none",
        },
    },
};
export default meta;

type Story = StoryObj<typeof Header>;

export const Component: Story = {
    args: {
        big: false,
    },
};

export const Index: Story = {
    name: "Index page",
    args: {
        big: true,
    },
};

import classNames from "classnames";
import styles from "./Footer.module.css";
import { Copyright } from "./part/Copyright.tsx";

type Props = {
    /** The additional className to attach. */
    className?: string;
};

export const Footer = ({ className }: Props) => {
    const classes = classNames(className, styles.footer);

    return (
        <footer className={classes}>
            <Copyright />
        </footer>
    );
};

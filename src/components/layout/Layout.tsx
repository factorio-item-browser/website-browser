import classNames from "classnames";
import { useAtomValue } from "jotai";
import { ReactNode } from "react";
import { breakpointAtom } from "../../state/breakpoint.ts";
import { Footer } from "./Footer.tsx";
import { Header } from "./Header.tsx";
import styles from "./Layout.module.css";
import { Sidebar } from "./Sidebar.tsx";

type Props = {
    /** Whether we are displaying the index page. */
    index?: boolean;
    /** The actual content to display in the main layout. */
    children: ReactNode;
};

export const Layout = ({ index, children }: Props) => {
    const breakpoint = useAtomValue(breakpointAtom);

    const classes = classNames(styles.container, `breakpoint-${breakpoint}`, {
        [styles.index]: index,
    });

    if (index) {
        return (
            <div className={classes}>
                <Header className={styles.header} big />
                <main className={styles.content}>{children}</main>
                <Footer className={styles.footer} />
            </div>
        );
    }

    return (
        <div className={classes}>
            <Header className={styles.header} />
            <Sidebar className={styles.sidebar} />
            <main className={styles.content}>{children}</main>
            <Footer className={styles.footer} />
        </div>
    );
};

import { mdiBug, mdiCubeOutline, mdiDatabasePlusOutline, mdiIceCream, mdiRocket, mdiViewComfy } from "@mdi/js";
import { useAtomValue, useSetAtom } from "jotai";
import { useTranslation } from "react-i18next";
import { SectionStyle } from "../../constants/section.ts";
import { repositoryAtom } from "../../state/combination.ts";
import {
    addEntityToFavoritesAtom,
    removeEntityFromFavoritesAtom,
    setFavoritesOrderAtom,
    sidebarFavoriteEntitiesAtom,
    sidebarLastViewedEntitiesAtom,
} from "../../state/sidebar.ts";
import { SidebarButton } from "../button/SidebarButton.tsx";
import { InternalLink } from "../link/InternalLink.tsx";
import { SidebarEntityList } from "../list/SidebarEntityList.tsx";
import { Section } from "../section/Section.tsx";

type Props = {
    /** The additional className to attach. */
    className?: string;
};

export const Sidebar = ({ className }: Props) => {
    const { t } = useTranslation("components");

    const favoriteEntities = useAtomValue(sidebarFavoriteEntitiesAtom);
    const lastViewedEntities = useAtomValue(sidebarLastViewedEntitiesAtom);

    const handleLastViewedClick = useSetAtom(addEntityToFavoritesAtom);
    const handleFavoriteClick = useSetAtom(removeEntityFromFavoritesAtom);
    const handleOrderChange = useSetAtom(setFavoritesOrderAtom);

    const repository = useAtomValue(repositoryAtom);

    return (
        <aside className={className}>
            <InternalLink route={"items"} box hover>
                <SidebarButton label={t("components:layout.sidebar.all-items")} iconPath={mdiViewComfy} />
            </InternalLink>

            <SidebarEntityList
                headline={t("components:layout.sidebar.favorites")}
                entities={favoriteEntities}
                onFavoriteClick={handleFavoriteClick}
                onOrderChange={handleOrderChange}
                favorites
            />

            <SidebarEntityList
                headline={t("components:layout.sidebar.last-viewed")}
                entities={lastViewedEntities}
                onFavoriteClick={handleLastViewedClick}
            />

            <Section headline={"Debug"} headIconPath={mdiBug} spacing style={SectionStyle.Sidebar}>
                <div
                    onClick={async () => {
                        await repository.initializeDatabase();
                    }}
                    style={{ marginBottom: "1px" }}
                >
                    <SidebarButton label={"Create database"} iconPath={mdiDatabasePlusOutline} />
                </div>

                <InternalLink route={"/2f4a45fa-a509-a9d1-aae6-ffcf984a7a76"} box hover>
                    <SidebarButton label={"Vanilla"} iconPath={mdiIceCream} />
                </InternalLink>
                <InternalLink route={"/3e533361-8e5e-028d-af0c-2a4bb35d4f2c"} box hover>
                    <SidebarButton label={"Space Exploration"} iconPath={mdiRocket} />
                </InternalLink>
                <InternalLink route={"/a9bb9f14-978e-cdf0-3ac8-b4d9ffda6c72"} box hover>
                    <SidebarButton label={"Ultra Cube"} iconPath={mdiCubeOutline} />
                </InternalLink>
            </Section>
        </aside>
    );
};

import { mdiMinus, mdiPlus } from "@mdi/js";
import classNames from "classnames";
import { ReactNode, useState } from "react";
import { useTranslation } from "react-i18next";
import { IconSize } from "../../constants/icon.ts";
import { SectionStyle } from "../../constants/section.ts";
import { TextSize } from "../../constants/text.ts";
import { ClickableIcon } from "../icon/ClickableIcon.tsx";
import { Icon } from "../icon/Icon.tsx";
import { Text } from "../text/Text.tsx";
import styles from "./Section.module.css";

type Props = {
    /** The headline of the section. */
    headline: string;
    /** The hash of the stylesheet icon to render in the head. */
    headIconHash?: string;
    /** The path of the Material Design icon to render in the head. */
    headIconPath?: string;
    /** Whether the section should support to be collapsed by the user. */
    collapsable?: boolean;
    /** The style of the component to use. */
    style?: SectionStyle;
    /** Whether to add a spacing to the top of the component. */
    spacing?: boolean;
    /** The elements to be arranged in a grid. */
    children?: ReactNode;
    /** The additional className to attach. */
    className?: string;
};

const headlineTextSizes = {
    [SectionStyle.Default]: TextSize.Headline,
    [SectionStyle.Sidebar]: TextSize.HeadlineSidebar,
    [SectionStyle.Title]: TextSize.Title,
};

export const Section = ({
    headline,
    headIconHash,
    headIconPath,
    collapsable = false,
    style = SectionStyle.Default,
    spacing = false,
    children,
    className,
}: Props) => {
    const { t } = useTranslation("common");
    const [collapsed, setCollapsed] = useState(false);

    const handleCollapseClick = () => {
        setCollapsed(!collapsed);
    };

    const classes = classNames(className, styles[`style-${style}`], {
        [styles.spacing]: spacing,
    });

    const label = t(`components:section.collapse-icon.${collapsed ? "expand" : "collapse"}`);
    const SectionTag = style === SectionStyle.Sidebar ? "div" : "section";
    const iconSize = style === SectionStyle.Title ? IconSize.Large : IconSize.Medium;

    return (
        <SectionTag className={classes}>
            <div className={styles.head}>
                <Icon className={styles.icon} hash={headIconHash} path={headIconPath} size={iconSize} />
                <Text className={styles.label} text={headline} size={headlineTextSizes[style]} />
                {collapsable ? (
                    <ClickableIcon
                        className={styles.collapse}
                        path={collapsed ? mdiPlus : mdiMinus}
                        title={label}
                        size={iconSize}
                        onClick={handleCollapseClick}
                    />
                ) : null}
            </div>
            {collapsable && collapsed ? null : children}
        </SectionTag>
    );
};

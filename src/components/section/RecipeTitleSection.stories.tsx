import type { Meta, StoryObj } from "@storybook/react";
import { exampleRecipes } from "../../../.storybook/examples.ts";
import { RecipeTitleSection } from "./RecipeTitleSection.tsx";

const meta: Meta<typeof RecipeTitleSection> = {
    title: "section/RecipeTitleSection",
    component: RecipeTitleSection,
};
export default meta;

type Story = StoryObj<typeof RecipeTitleSection>;

export const Component: Story = {
    args: {
        recipe: exampleRecipes.electronicCircuit,
    },
};

export const Description: Story = {
    name: "With Description",
    args: {
        recipe: exampleRecipes.copperCable,
        spacing: false,
    },
};

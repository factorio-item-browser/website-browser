import { mdiEarth } from "@mdi/js";
import type { Meta, StoryObj } from "@storybook/react";
import { SectionStyle } from "../../constants/section.ts";
import { Card } from "../card/part/Card.tsx";
import { Section } from "./Section.tsx";

const meta: Meta<typeof Section> = {
    title: "section/Section",
    component: Section,
    parameters: {
        backgrounds: {
            default: "none",
        },
    },
};
export default meta;

const children = (
    <Card spacing>
        <div style={{ padding: "8px" }}>
            Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam sed sem nec augue facilisis posuere. Class
            aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Sed ut turpis vel
            turpis aliquet pharetra. Etiam non dictum mi. Nulla hendrerit elit ut quam tincidunt tincidunt. Donec quam
            augue, consequat quis blandit sit amet, finibus eu dolor. Suspendisse at mattis velit. Donec imperdiet
            mattis velit at posuere. Maecenas ultrices cursus dapibus.
        </div>
    </Card>
);

type Story = StoryObj<typeof Section>;

export const Component: Story = {
    args: {
        headline: "Section headline",
        headIconPath: mdiEarth,
        collapsable: false,
        spacing: false,
        style: SectionStyle.Default,
        children: children,
    },
};

export const Collapsable: Story = {
    args: {
        headline: "Collapsable section",
        headIconPath: mdiEarth,
        collapsable: true,
        spacing: false,
        style: SectionStyle.Default,
        children: children,
    },
};

export const Styles: Story = {
    name: "Different styles",
    render: () => (
        <>
            <Section headline={"SectionStyle.Default"} headIconPath={mdiEarth} style={SectionStyle.Default}>
                {children}
            </Section>
            <Section headline={"SectionStyle.Title"} headIconPath={mdiEarth} style={SectionStyle.Title} spacing>
                {children}
            </Section>
            <Section headline={"SectionStyle.Sidebar"} headIconPath={mdiEarth} style={SectionStyle.Sidebar} spacing>
                {children}
            </Section>
        </>
    ),
};

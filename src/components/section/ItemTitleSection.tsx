import { mdiCodeBrackets, mdiConsoleLine, mdiText } from "@mdi/js";
import { useTranslation } from "react-i18next";
import { ItemType } from "../../constants/entity.ts";
import { SectionStyle } from "../../constants/section.ts";
import { Item } from "../../model/entity/item.ts";
import { CopyTemplateCard } from "../card/CopyTemplateCard.tsx";
import { TextCard } from "../card/TextCard.tsx";
import { Section } from "./Section.tsx";

type Props = {
    /** The ite to render the title section for. */
    item: Item;
    /** Whether to add a spacing to the top of the component. */
    spacing?: boolean;
    /** The additional className to attach. */
    className?: string;
};

export const ItemTitleSection = ({ item, spacing, className }: Props) => {
    const { t } = useTranslation(["common", "components"]);

    let copyTemplateValue = `[${item.type}=${item.name}]`;
    if (item.type === ItemType.Resource) {
        copyTemplateValue = `[entity=${item.name}]`;
    }

    const headLabel = t("components:section.item-title-section.headline", {
        type: t(`common:entity-type.${item.type}`),
        label: item.label,
    });

    return (
        <Section
            className={className}
            headline={headLabel}
            headIconHash={item.iconHash}
            style={SectionStyle.Title}
            spacing={spacing}
        >
            {item.description ? <TextCard text={item.description} iconPath={mdiText} spacing /> : null}

            <CopyTemplateCard
                iconPath={mdiCodeBrackets}
                label={t("common:copy-template.icon.label")}
                value={copyTemplateValue}
                description={t("common:copy-template.icon.description")}
                spacing
            />

            {item.type === ItemType.Item ? (
                <CopyTemplateCard
                    iconPath={mdiConsoleLine}
                    label={t("common:copy-template.cheat.label")}
                    value={`/c game.player.insert{ name="${item.name}", count=10 }`}
                    description={t("common:copy-template.cheat.description")}
                    spacing
                />
            ) : null}
        </Section>
    );
};

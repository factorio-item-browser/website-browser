import { mdiCodeBrackets, mdiText } from "@mdi/js";
import { useTranslation } from "react-i18next";
import { RecipeType } from "../../constants/entity.ts";
import { SectionStyle } from "../../constants/section.ts";
import { Recipe } from "../../model/entity/recipe.ts";
import { CopyTemplateCard } from "../card/CopyTemplateCard.tsx";
import { TextCard } from "../card/TextCard.tsx";
import { Section } from "./Section.tsx";

type Props = {
    /** The recipe to render the title section for. */
    recipe: Recipe;
    /** Whether to add a spacing to the top of the component. */
    spacing?: boolean;
    /** The additional className to attach. */
    className?: string;
};

export const RecipeTitleSection = ({ recipe, spacing, className }: Props) => {
    const { t } = useTranslation(["common", "components"]);

    let copyTemplateValue = `[recipe=${recipe.name}]`;
    if (recipe.type === RecipeType.Mining) {
        copyTemplateValue = `[entity=${recipe.name}]`;
    } else if (recipe.type === RecipeType.RocketLaunch) {
        copyTemplateValue = `[item=${recipe.name}]`;
    }

    const headLabel = t("components:section.recipe-title-section.headline", {
        type: t(`common:entity-type.${recipe.type}`),
        label: recipe.label,
    });

    return (
        <Section
            className={className}
            headline={headLabel}
            headIconHash={recipe.iconHash}
            style={SectionStyle.Title}
            spacing={spacing}
        >
            {recipe.description ? <TextCard text={recipe.description} iconPath={mdiText} spacing /> : null}

            <CopyTemplateCard
                iconPath={mdiCodeBrackets}
                label={t("common:copy-template.icon.label")}
                value={copyTemplateValue}
                description={t("common:copy-template.icon.description")}
                spacing
            />
        </Section>
    );
};

import type { Meta, StoryObj } from "@storybook/react";
import { exampleMachines } from "../../../.storybook/examples.ts";
import { MachineTitleSection } from "./MachineTitleSection.tsx";

const meta: Meta<typeof MachineTitleSection> = {
    title: "section/MachineTitleSection",
    component: MachineTitleSection,
};
export default meta;

type Story = StoryObj<typeof MachineTitleSection>;

export const Component: Story = {
    args: {
        machine: exampleMachines.assemblingMachine3,
        spacing: false,
    },
};

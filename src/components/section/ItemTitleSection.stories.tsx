import type { Meta, StoryObj } from "@storybook/react";
import { exampleItems } from "../../../.storybook/examples.ts";
import { ItemTitleSection } from "./ItemTitleSection.tsx";

const meta: Meta<typeof ItemTitleSection> = {
    title: "section/ItemTitleSection",
    component: ItemTitleSection,
};
export default meta;

type Story = StoryObj<typeof ItemTitleSection>;

export const Component: Story = {
    args: {
        item: exampleItems.electronicCircuit,
        spacing: false,
    },
};

export const Description: Story = {
    name: "With Description",
    args: {
        item: exampleItems.copperCable,
        spacing: false,
    },
};

export const Fluid: Story = {
    args: {
        item: exampleItems.crudeOil,
        spacing: false,
    },
};

export const Resource: Story = {
    args: {
        item: exampleItems.ironOre,
        spacing: false,
    },
};

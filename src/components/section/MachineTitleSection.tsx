import { mdiCodeBrackets, mdiText } from "@mdi/js";
import { useTranslation } from "react-i18next";
import { SectionStyle } from "../../constants/section.ts";
import { Machine } from "../../model/entity/machine.ts";
import { CopyTemplateCard } from "../card/CopyTemplateCard.tsx";
import { TextCard } from "../card/TextCard.tsx";
import { Section } from "./Section.tsx";

type Props = {
    /** The machine to render the title section for. */
    machine: Machine;
    /** Whether to add a spacing to the top of the component. */
    spacing?: boolean;
    /** The additional className to attach. */
    className?: string;
};

export const MachineTitleSection = ({ machine, spacing, className }: Props) => {
    const { t } = useTranslation(["common", "components"]);

    const headLabel = t("components:section.machine-title-section.headline", {
        type: t(`common:entity-type.${machine.type}`),
        label: machine.label,
    });

    return (
        <Section
            className={className}
            headline={headLabel}
            headIconHash={machine.iconHash}
            style={SectionStyle.Title}
            spacing={spacing}
        >
            {machine.description ? <TextCard text={machine.description} iconPath={mdiText} spacing /> : null}

            <CopyTemplateCard
                iconPath={mdiCodeBrackets}
                label={t("common:copy-template.icon.label")}
                value={`[entity=${machine.name}]`}
                description={t("common:copy-template.icon.description")}
                spacing
            />
        </Section>
    );
};

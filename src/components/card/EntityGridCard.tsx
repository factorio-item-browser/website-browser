import classNames from "classnames";
import { IconSize } from "../../constants/icon.ts";
import { useScrollObserver } from "../../hooks/pagination.ts";
import { Entity } from "../../model/entity/entity.ts";
import { Icon } from "../icon/Icon.tsx";
import { EntityLink } from "../link/EntityLink.tsx";
import styles from "./EntityGridCard.module.css";
import { Card } from "./part/Card.tsx";

type Props = {
    /** The entities to show in the grid. */
    entities: Entity[];
    /** The handler for when the end of the grid has moved into the viewport and thus the next page should be added. */
    onNextPage?: () => void;
    /** Whether to add a minimal spacing to the card. */
    spacing?: boolean;
    /** The additional className to attach. */
    className?: string;
};

export const EntityGridCard = ({ entities, onNextPage, spacing, className }: Props) => {
    const classes = classNames(className, styles.grid);
    const observerTarget = useScrollObserver(
        () => {
            onNextPage && onNextPage();
        },
        {
            threshold: 0.01,
            rootMargin: "20%",
        },
    );

    return (
        <Card className={classes} spacing={spacing}>
            {entities.map((entity) => (
                <EntityLink key={entity.key} entity={entity} box>
                    <Icon hash={entity.iconHash} size={IconSize.Large} />
                </EntityLink>
            ))}
            <div ref={observerTarget} />
        </Card>
    );
};

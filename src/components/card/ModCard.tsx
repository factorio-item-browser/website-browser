import { mdiOpenInNew } from "@mdi/js";
import { useTranslation } from "react-i18next";
import { IconSize } from "../../constants/icon.ts";
import { TextSize } from "../../constants/text.ts";
import { Mod } from "../../model/entity/mod.ts";
import { Icon } from "../icon/Icon.tsx";
import { EntityLink } from "../link/EntityLink.tsx";
import { Text } from "../text/Text.tsx";
import styles from "./ModCard.module.css";
import { Card } from "./part/Card.tsx";
import { CardLine } from "./part/CardLine.tsx";
import { ModCardDetail } from "./part/ModCardDetail.tsx";

type Props = {
    /** The mod to be displayed. */
    mod: Mod;
    /** The additional className to attach. */
    className?: string;
};

export const ModCard = ({ mod, className }: Props) => {
    const { t } = useTranslation("components");

    return (
        <EntityLink className={className} entity={mod} box hover>
            <Card className={styles.card}>
                <Icon className={styles.icon} hash={mod.iconHash} size={IconSize.Huge} />
                <Text className={styles.label} text={mod.label} size={TextSize.Large} nowrap />
                <CardLine className={styles.line} />
                <div className={styles.details}>
                    <ModCardDetail label={t("components:card.mod-card.name")} value={mod.name} />
                    <ModCardDetail label={t("components:card.mod-card.author")} value={mod.author} />
                    <ModCardDetail label={t("components:card.mod-card.version")} value={mod.version} />
                </div>
                <Icon className={styles.external} path={mdiOpenInNew} size={IconSize.Small} />
            </Card>
        </EntityLink>
    );
};

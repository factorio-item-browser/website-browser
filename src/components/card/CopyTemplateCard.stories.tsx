import { mdiContentCopy } from "@mdi/js";
import type { Meta, StoryObj } from "@storybook/react";
import { CopyTemplateCard } from "./CopyTemplateCard.tsx";

const meta: Meta<typeof CopyTemplateCard> = {
    title: "card/CopyTemplateCard",
    component: CopyTemplateCard,
};
export default meta;

type Story = StoryObj<typeof CopyTemplateCard>;

export const Component: Story = {
    args: {
        label: "Fancy text to copy:",
        value: "[The Thing to copy]",
        description: "This is a fancy thing you can easily copy by clicking it.",
        iconPath: mdiContentCopy,
        spacing: false,
    },
};

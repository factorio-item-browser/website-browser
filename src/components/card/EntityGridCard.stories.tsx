import type { Meta, StoryObj } from "@storybook/react";
import { ComponentProps } from "react";
import { ItemType } from "../../constants/entity.ts";
import { EntityGridCard } from "./EntityGridCard.tsx";

type ExtendedArgs = ComponentProps<typeof EntityGridCard> & {
    count: number;
};

const meta: Meta<ExtendedArgs> = {
    title: "card/EntityGridCard",
    component: EntityGridCard,
    argTypes: {
        count: {
            name: "count",
            type: { name: "number", required: true },
            description: "The number of items to render for the preview. (Not used in the actual component.)",
            defaultValue: 128,
            table: {
                category: "Storybook preview",
            },
        },
    },
    render: ({ count }) => {
        const entities = [...Array(count).keys()].map(() => {
            return {
                key: "item.electronic-circuit",
                type: ItemType.Item,
                name: "electronic-circuit",
                label: "Electronic circuit",
                description: "",
                iconHash: "item-electronic-circuit",
            };
        });
        return <EntityGridCard entities={entities} />;
    },
};
export default meta;

type Story = StoryObj<ExtendedArgs>;

export const Component: Story = {
    args: {
        count: 128,
    },
};

import classNames from "classnames";
import { useTranslation } from "react-i18next";
import { IconSize } from "../../constants/icon.ts";
import { TextSize, TextStyle } from "../../constants/text.ts";
import { RecipeWithTechnology } from "../../model/entity/recipe.ts";
import { Icon } from "../icon/Icon.tsx";
import { EntityLink } from "../link/EntityLink.tsx";
import { CompactRecipe } from "../recipe/CompactRecipe.tsx";
import { Text } from "../text/Text.tsx";
import styles from "./CompactRecipeCard.module.css";
import { Card } from "./part/Card.tsx";

type Props = {
    /** The recipe to display. */
    recipe: RecipeWithTechnology;
    /** Whether to add a minimal spacing to the card. */
    spacing?: boolean;
    /** The additional className to attach. */
    className?: string;
};

export const CompactRecipeCard = ({ recipe, spacing, className }: Props) => {
    const { t } = useTranslation("common");
    const classes = classNames(className, styles.recipe);

    return (
        <Card className={classes} spacing={spacing}>
            <EntityLink className={styles.link} entity={recipe} box hover>
                <Icon className={styles.icon} hash={recipe.iconHash} size={IconSize.Medium} />
                <Text
                    className={styles.type}
                    text={t(`common:entity-type.${recipe.type}`)}
                    size={TextSize.Small}
                    style={TextStyle.Uppercase}
                    nowrap
                />
                <Text className={styles.label} text={recipe.label} size={TextSize.Large} nowrap />
            </EntityLink>
            <CompactRecipe className={styles.details} recipe={recipe} />
            <Icon className={styles.tech} hash={recipe.technology?.iconHash} size={IconSize.Large} />
        </Card>
    );
};

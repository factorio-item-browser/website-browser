import { mdiCards, mdiLayers, mdiLightningBolt, mdiSpeedometer, mdiTimerOutline, mdiWater } from "@mdi/js";
import { useTranslation } from "react-i18next";
import { RecipeType } from "../../constants/entity.ts";
import { IconSize } from "../../constants/icon.ts";
import { TextSize } from "../../constants/text.ts";
import { formatCraftingSpeed, formatEnergyUsage, formatMachineSlots } from "../../format/machine.ts";
import { formatCraftingTime } from "../../format/recipe.ts";
import { Machine } from "../../model/entity/machine.ts";
import { Recipe } from "../../model/entity/recipe.ts";
import { Icon } from "../icon/Icon.tsx";
import { EntityLink } from "../link/EntityLink.tsx";
import { Text } from "../text/Text.tsx";
import styles from "./MachineCard.module.css";
import { Card } from "./part/Card.tsx";
import { CardLine } from "./part/CardLine.tsx";
import { MachineCardDetail } from "./part/MachineCardDetail.tsx";

type Props = {
    /** The machine do display in the card. */
    machine: Machine;
    /** The recipe to adjust the machine card for. */
    recipe: Recipe;
    /** The additional className to attach. */
    className?: string;
};

export const MachineCard = ({ machine, recipe, className }: Props) => {
    const { t } = useTranslation("components");

    const details = [];

    if (recipe.type === RecipeType.Mining) {
        details.push(
            ["mining-speed", mdiSpeedometer, formatCraftingSpeed(machine.speed)],
            ["mining-time", mdiTimerOutline, formatCraftingTime(1 / machine.speed)],
            ["energy-usage", mdiLightningBolt, formatEnergyUsage(machine.energyUsage, machine.energyUsageUnit)],
            ["storage-slots", mdiLayers, formatMachineSlots(machine.name === "character" ? 0 : 1)],
            ["fluid-slots", mdiWater, formatMachineSlots(machine.fluidInputSlots)],
            ["module-slots", mdiCards, formatMachineSlots(machine.moduleSlots)],
        );
    } else {
        details.push(
            ["crafting-speed", mdiSpeedometer, formatCraftingSpeed(machine.speed)],
            ["crafting-time", mdiTimerOutline, formatCraftingTime(recipe.time / machine.speed)],
            ["energy-usage", mdiLightningBolt, formatEnergyUsage(machine.energyUsage, machine.energyUsageUnit)],
            ["item-slots", mdiLayers, formatMachineSlots(machine.itemSlots)],
            ["fluid-slots", mdiWater, formatMachineSlots(machine.fluidInputSlots)],
            ["module-slots", mdiCards, formatMachineSlots(machine.moduleSlots)],
        );
    }

    return (
        <EntityLink className={className} entity={machine} box hover>
            <Card>
                <div className={styles.head}>
                    <Icon className={styles.icon} hash={machine.iconHash} size={IconSize.Medium} />
                    <Text className={styles.label} text={machine.label} size={TextSize.Large} nowrap />
                </div>
                <CardLine />
                <div className={styles.details}>
                    {details.map(([name, icon, value]) => (
                        <MachineCardDetail
                            key={name}
                            label={t(`components:card.machine-card.detail.${name}`)}
                            value={value}
                            iconPath={icon}
                        />
                    ))}
                </div>
            </Card>
        </EntityLink>
    );
};

import type { Meta, StoryObj } from "@storybook/react";
import { exampleRecipes } from "../../../.storybook/examples.ts";
import { RecipeProductsCard } from "./RecipeProductsCard.tsx";

const meta: Meta<typeof RecipeProductsCard> = {
    title: "card/RecipeProductsCard",
    component: RecipeProductsCard,
};
export default meta;

type Story = StoryObj<typeof RecipeProductsCard>;

export const Component: Story = {
    args: {
        recipe: exampleRecipes.electronicCircuit,
    },
};

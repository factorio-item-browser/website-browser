import type { Meta, StoryObj } from "@storybook/react";
import { ErrorCard } from "./ErrorCard.tsx";

const meta: Meta<typeof ErrorCard> = {
    title: "card/ErrorCard",
    component: ErrorCard,
};
export default meta;

type Story = StoryObj<typeof ErrorCard>;

export const Component: Story = {
    args: {
        title: "Error Title",
        message: "Lorem ipsum dolor sit amet.",
        spacing: false,
    },
};

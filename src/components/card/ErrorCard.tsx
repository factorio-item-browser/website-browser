import classNames from "classnames";
import { TextSize, TextStyle } from "../../constants/text.ts";
import { Text } from "../text/Text.tsx";
import styles from "./ErrorCard.module.css";
import { Card } from "./part/Card.tsx";

type Props = {
    /** The title of the error card. */
    title: string;
    /** The message of the error card. */
    message: string;
    /** Whether to add a minimal spacing to the card. */
    spacing?: boolean;
    /** The additional className to attach. */
    className?: string;
};

export const ErrorCard = ({ title, message, spacing, className }: Props) => {
    const classes = classNames(styles.card, className);

    return (
        <Card className={classes} spacing={spacing}>
            <div className={styles.machine} />
            <Text className={styles.title} text={title} size={TextSize.Title} />
            <Text className={styles.message} text={message} style={TextStyle.Bold} />
        </Card>
    );
};

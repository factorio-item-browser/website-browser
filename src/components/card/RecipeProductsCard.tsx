import { mdiUpload } from "@mdi/js";
import { ReactNode } from "react";
import { useTranslation } from "react-i18next";
import { formatAmount } from "../../format/recipe.ts";
import { Recipe } from "../../model/entity/recipe.ts";
import { EntityLink } from "../link/EntityLink.tsx";
import { Card } from "./part/Card.tsx";
import { CardHead } from "./part/CardHead.tsx";
import { CardLine } from "./part/CardLine.tsx";
import { RecipeItemsCardLine } from "./part/RecipeItemsCardLine.tsx";

type Props = {
    /** The recipe of which to display the products as a card. */
    recipe: Recipe;
    /** The additional className to attach. */
    className?: string;
};

export const RecipeProductsCard = ({ recipe, className }: Props) => {
    const { t } = useTranslation("components");
    const lines: ReactNode[] = [];

    recipe.products.forEach((product) => {
        lines.push(
            <EntityLink key={product.key} entity={product} box hover>
                <RecipeItemsCardLine
                    label={product.label}
                    amount={formatAmount(product.amount)}
                    iconHash={product.iconHash}
                />
            </EntityLink>,
        );
    });

    return (
        <Card className={className}>
            <CardHead label={t("components:card.recipe-products-card.head")} iconPath={mdiUpload} />
            {lines.reduce((prev, curr, idx) => [prev, <CardLine key={idx} />, curr])}
        </Card>
    );
};

import { useCallback, useRef } from "react";
import { TextSize, TextStyle } from "../../constants/text.ts";
import { Icon } from "../icon/Icon.tsx";
import { Text } from "../text/Text.tsx";
import styles from "./CopyTemplateCard.module.css";
import { Card } from "./part/Card.tsx";

type Props = {
    /** The label of the copy template. */
    label: string;
    /** The value to make copiable in the template. */
    value: string;
    /** The description to the value. */
    description: string;
    /** The hash of the stylesheet icon to render in the card. */
    iconHash?: string;
    /** The path of the Material Design icon to render in the card. */
    iconPath?: string;
    /** Whether to apply a spacing to the top of the card. */
    spacing?: boolean;
    /** The additional className to attach. */
    className?: string;
};

export const CopyTemplateCard = ({ label, value, description, iconHash, iconPath, spacing, className }: Props) => {
    const ref = useRef<HTMLSpanElement>(null);
    const handleClick = useCallback(() => {
        const selection = window.getSelection();
        if (ref.current && selection) {
            const range = document.createRange();
            range.selectNodeContents(ref.current);

            selection.removeAllRanges();
            selection.addRange(range);
        }
    }, [ref]);

    return (
        <div className={className} onClick={handleClick}>
            <Card className={styles.card} spacing={spacing}>
                <Icon className={styles.icon} hash={iconHash} path={iconPath} />
                <Text className={styles.label} text={label} style={TextStyle.Bold} />
                <Text className={styles.value} text={value} style={TextStyle.Monospace} ref={ref} />
                <Text className={styles.description} text={description} size={TextSize.Small} />
            </Card>
        </div>
    );
};

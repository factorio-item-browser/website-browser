import classNames from "classnames";
import { Icon } from "../icon/Icon.tsx";
import { Text } from "../text/Text.tsx";
import styles from "./TextCard.module.css";
import { Card } from "./part/Card.tsx";

type Props = {
    /** The text to show in the card. */
    text: string;
    /** The hash of the stylesheet icon to render in the card. */
    iconHash?: string;
    /** The path of the Material Design icon to render in the card. */
    iconPath?: string;
    /** Whether to apply a spacing to the top of the card. */
    spacing?: boolean;
    /** The additional className to attach. */
    className?: string;
};

export const TextCard = ({ text, iconHash, iconPath, spacing, className }: Props) => {
    const classes = classNames(className, styles.card);

    return (
        <Card className={classes} spacing={spacing}>
            <Icon className={styles.icon} hash={iconHash} path={iconPath} />
            <Text className={styles.text} text={text} />
        </Card>
    );
};

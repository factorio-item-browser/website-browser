import type { Meta, StoryObj } from "@storybook/react";
import { exampleRecipes } from "../../../.storybook/examples.ts";
import { RecipeIngredientsCard } from "./RecipeIngredientsCard.tsx";

const meta: Meta<typeof RecipeIngredientsCard> = {
    title: "card/RecipeIngredientsCard",
    component: RecipeIngredientsCard,
};
export default meta;

type Story = StoryObj<typeof RecipeIngredientsCard>;

export const Component: Story = {
    args: {
        recipe: exampleRecipes.electronicCircuit,
    },
};

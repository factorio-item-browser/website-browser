import { mdiDownload, mdiTimerOutline } from "@mdi/js";
import { ReactNode } from "react";
import { useTranslation } from "react-i18next";
import { RecipeType } from "../../constants/entity.ts";
import { formatAmount, formatCraftingTime } from "../../format/recipe.ts";
import { Recipe } from "../../model/entity/recipe.ts";
import { EntityLink } from "../link/EntityLink.tsx";
import { Card } from "./part/Card.tsx";
import { CardHead } from "./part/CardHead.tsx";
import { CardLine } from "./part/CardLine.tsx";
import { RecipeItemsCardLine } from "./part/RecipeItemsCardLine.tsx";

type Props = {
    /** The recipe of which to display the ingredients as a card. */
    recipe: Recipe;
    /** The additional className to attach. */
    className?: string;
};

export const RecipeIngredientsCard = ({ recipe, className }: Props) => {
    const { t } = useTranslation("components");
    const lines: ReactNode[] = [];

    if (recipe.type === RecipeType.Crafting && recipe.time > 0) {
        lines.push(
            <RecipeItemsCardLine
                key={"crafting-time"}
                label={t("components:card.recipe-ingredients-card.crafting-time")}
                amount={formatCraftingTime(recipe.time)}
                iconPath={mdiTimerOutline}
            />,
        );
    }

    recipe.ingredients.forEach((ingredient) => {
        lines.push(
            <EntityLink key={ingredient.key} entity={ingredient} box hover>
                <RecipeItemsCardLine
                    label={ingredient.label}
                    amount={formatAmount(ingredient.amount)}
                    iconHash={ingredient.iconHash}
                />
            </EntityLink>,
        );
    });

    return (
        <Card className={className}>
            <CardHead label={t("components:card.recipe-ingredients-card.head")} iconPath={mdiDownload} />
            {lines.reduce((prev, curr, idx) => [prev, <CardLine key={idx} />, curr])}
        </Card>
    );
};

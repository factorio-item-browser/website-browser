import type { Meta, StoryObj } from "@storybook/react";
import { exampleMods } from "../../../.storybook/examples.ts";
import { ModCard } from "./ModCard.tsx";

const meta: Meta<typeof ModCard> = {
    title: "card/ModCard",
    component: ModCard,
};
export default meta;

type Story = StoryObj<typeof ModCard>;

export const Component: Story = {
    args: {
        mod: exampleMods.spaceExploration,
    },
};

export const Overflow: Story = {
    args: {
        mod: {
            ...exampleMods.base,
            label: "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas dictum elementum ligula. Integer imperdiet ipsum quis erat vestibulum commodo. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia curae; Duis efficitur leo a mi laoreet posuere.",
            author: "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas dictum elementum ligula. Integer imperdiet ipsum quis erat vestibulum commodo. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia curae; Duis efficitur leo a mi laoreet posuere.",
        },
    },
};

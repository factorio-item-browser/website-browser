import type { Meta, StoryObj } from "@storybook/react";
import { Card } from "./Card.tsx";
import { CardText } from "./CardText.tsx";

const meta: Meta<typeof CardText> = {
    title: "card/part/CardText",
    component: CardText,
};
export default meta;

type Story = StoryObj<typeof CardText>;

const text = `Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec venenatis dictum sem at finibus. Sed 
malesuada vel nisl eu consectetur. Vestibulum eu hendrerit lacus. Aliquam nunc mi, tincidunt sit amet feugiat sed, 
finibus id ligula.`;

export const Component: Story = {
    args: {
        text: text,
    },
    render: (args) => (
        <Card>
            <CardText {...args} />
        </Card>
    ),
};

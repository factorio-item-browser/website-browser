import classNames from "classnames";
import { useCallback, useRef } from "react";
import styles from "./CardCopyTemplate.module.css";

type Props = {
    /** The label to display in the copy template. */
    label: string;
    /** The label to make able to be copied. */
    value: string;
    /** The description explaining the value. */
    description: string;
    /** The additional className to attach. */
    className?: string;
};

export const CardCopyTemplate = ({ label, value, description, className }: Props) => {
    const classes = classNames(className, styles.template);

    const ref = useRef<HTMLSpanElement>(null);
    const handleClick = useCallback(() => {
        const selection = window.getSelection();
        if (ref.current && selection) {
            const range = document.createRange();
            range.selectNodeContents(ref.current);

            selection.removeAllRanges();
            selection.addRange(range);
        }
    }, [ref]);

    return (
        <div className={classes} onClick={handleClick}>
            <span className={styles.label}>{label}</span>
            <span className={styles.value} ref={ref}>
                {value}
            </span>
            <span className={styles.description}>{description}</span>
        </div>
    );
};

import classNames from "classnames";
import styles from "./CardText.module.css";

type Props = {
    /** The text to display. */
    text: string;
    /** The additional className to attach. */
    className?: string;
};

export const CardText = ({ text, className }: Props) => {
    const classes = classNames(className, styles.text);

    return <div className={classes}>{text}</div>;
};

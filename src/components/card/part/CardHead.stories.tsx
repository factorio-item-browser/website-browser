import type { Meta, StoryObj } from "@storybook/react";
import { HeadStyle } from "../../../constants/head.ts";
import { Card } from "./Card.tsx";
import { CardHead } from "./CardHead.tsx";

const meta: Meta<typeof CardHead> = {
    title: "card/part/CardHead",
    component: CardHead,
    render: (args) => (
        <Card>
            <CardHead {...args} />
        </Card>
    ),
};
export default meta;

type Story = StoryObj<typeof CardHead>;

export const Component: Story = {
    args: {
        label: "Electronic circuit",
        style: HeadStyle.Default,
        iconHash: "item-electronic-circuit",
    },
};

export const Styles: Story = {
    name: "Different styles",
    render: () => (
        <>
            <Card>
                <CardHead label={"HeadStyle.Default"} style={HeadStyle.Default} iconHash={"item-electronic-circuit"} />
            </Card>
            <div style={{ height: "1rem" }} />
            <Card>
                <CardHead label={"HeadStyle.Title"} style={HeadStyle.Title} iconHash={"item-electronic-circuit"} />
            </Card>
        </>
    ),
};

import classNames from "classnames";
import { Text } from "../../text/Text.tsx";
import styles from "./ModCardDetail.module.css";

type Props = {
    /** The label of the detail to display. */
    label: string;
    /** The value of the detail to display. */
    value: string;
    /** The additional className to attach. */
    className?: string;
};

export const ModCardDetail = ({ label, value, className }: Props) => {
    const classes = classNames(className, styles.detail);

    return (
        <div className={classes}>
            <Text className={styles.label} text={label} nowrap />
            <Text className={styles.value} text={value} nowrap />
        </div>
    );
};

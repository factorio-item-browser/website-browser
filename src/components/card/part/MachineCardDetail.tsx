import classNames from "classnames";
import { IconSize } from "../../../constants/icon.ts";
import { TextStyle } from "../../../constants/text.ts";
import { Icon } from "../../icon/Icon.tsx";
import { Text } from "../../text/Text.tsx";
import styles from "./MachineCardDetail.module.css";

type Props = {
    /** The label of the detail to display. */
    label: string;
    /** The value of the detail to display. */
    value: string;
    /** The hash of the stylesheet icon to render. */
    iconHash?: string;
    /** The path of the Material Design icon to render. */
    iconPath?: string;
    /** The additional className to attach. */
    className?: string;
};

export const MachineCardDetail = ({ label, value, iconHash, iconPath, className }: Props) => {
    const classes = classNames(className, styles.detail);

    return (
        <div className={classes}>
            <Icon className={styles.icon} hash={iconHash} path={iconPath} size={IconSize.Tiny} />
            <Text className={styles.label} text={label} nowrap />
            <Text className={styles.value} text={value} style={TextStyle.Bold} nowrap />
        </div>
    );
};

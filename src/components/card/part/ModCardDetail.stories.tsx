import type { Meta, StoryObj } from "@storybook/react";
import { ModCardDetail } from "./ModCardDetail.tsx";

const meta: Meta<typeof ModCardDetail> = {
    title: "card/part/ModCardDetail",
    component: ModCardDetail,
};
export default meta;

type Story = StoryObj<typeof ModCardDetail>;

export const Component: Story = {
    args: {
        label: "Author:",
        value: "Factorio team",
    },
};

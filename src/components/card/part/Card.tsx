import classNames from "classnames";
import { ReactNode } from "react";
import styles from "./Card.module.css";

type Props = {
    /** Whether to add a minimal spacing to the card. */
    spacing?: boolean;
    /** The additional className to attach. */
    className?: string;
    /** The children of the card. */
    children: ReactNode;
};

export const Card = ({ spacing, className, children }: Props) => {
    const classes = classNames(className, styles.card, {
        [styles.spacing]: spacing,
    });

    return <div className={classes}>{children}</div>;
};

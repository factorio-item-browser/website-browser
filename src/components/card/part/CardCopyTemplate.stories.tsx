import type { Meta, StoryObj } from "@storybook/react";
import { Card } from "./Card.tsx";
import { CardCopyTemplate } from "./CardCopyTemplate.tsx";
import { CardLine } from "./CardLine.tsx";

const meta: Meta<typeof CardCopyTemplate> = {
    title: "card/part/CardCopyTemplate",
    component: CardCopyTemplate,
    render: (args) => (
        <Card>
            <CardCopyTemplate {...args} />
        </Card>
    ),
};
export default meta;

type Story = StoryObj<typeof CardCopyTemplate>;

export const Component: Story = {
    args: {
        label: "Fancy text to copy:",
        value: "[The Thing to copy]",
        description: "This is a fancy thing you can easily copy by clicking it.",
    },
};

export const Multiple: Story = {
    name: "Multiple templates",
    render: () => (
        <Card>
            <CardCopyTemplate label={"Template 1:"} value={"[Value 1]"} description={"Description 1"} />
            <CardLine />
            <CardCopyTemplate label={"Template 2:"} value={"/Value 2"} description={"Description 2"} />
        </Card>
    ),
};

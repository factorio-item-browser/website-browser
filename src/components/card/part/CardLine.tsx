import classNames from "classnames";
import styles from "./CardLine.module.css";

type Props = {
    /** The additional className to attach. */
    className?: string;
};

export const CardLine = ({ className }: Props) => {
    const classes = classNames(className, styles.line);

    return <div className={classes} />;
};

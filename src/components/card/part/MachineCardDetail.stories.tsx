import { mdiSpeedometer } from "@mdi/js";
import type { Meta, StoryObj } from "@storybook/react";
import { MachineCardDetail } from "./MachineCardDetail.tsx";

const meta: Meta<typeof MachineCardDetail> = {
    title: "card/part/MachineCardDetail",
    component: MachineCardDetail,
};
export default meta;

type Story = StoryObj<typeof MachineCardDetail>;

export const Component: Story = {
    args: {
        label: "Crafting speed:",
        value: "42x",
        iconPath: mdiSpeedometer,
    },
};

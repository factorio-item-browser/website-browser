import type { Meta, StoryObj } from "@storybook/react";
import { Card } from "./Card.tsx";
import { CardLine } from "./CardLine.tsx";
import { CardText } from "./CardText.tsx";

const meta: Meta<typeof CardLine> = {
    title: "card/part/CardLine",
    component: CardLine,
};
export default meta;

type Story = StoryObj<typeof CardLine>;

export const Component: Story = {
    render: () => (
        <Card>
            <CardText text={""} />
            <CardLine />
            <CardText text={""} />
        </Card>
    ),
};

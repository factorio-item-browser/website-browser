import type { Meta, StoryObj } from "@storybook/react";
import { RecipeItemsCardLine } from "./RecipeItemsCardLine.tsx";

const meta: Meta<typeof RecipeItemsCardLine> = {
    title: "card/part/RecipeItemsCardLine",
    component: RecipeItemsCardLine,
};
export default meta;

type Story = StoryObj<typeof RecipeItemsCardLine>;

export const Component: Story = {
    args: {
        label: "Electronic circuit",
        amount: "42x",
        iconHash: "item-electronic-circuit",
    },
};

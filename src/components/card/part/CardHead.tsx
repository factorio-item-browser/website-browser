import classNames from "classnames";
import { HeadStyle } from "../../../constants/head.ts";
import { IconSize } from "../../../constants/icon.ts";
import { TextSize } from "../../../constants/text.ts";
import { Icon } from "../../icon/Icon.tsx";
import { Text } from "../../text/Text.tsx";
import styles from "./CardHead.module.css";

type Props = {
    /** The label of the card head. */
    label: string;
    /** The style of the card head. */
    style?: HeadStyle;
    /** The hash of the stylesheet icon to render. */
    iconHash?: string;
    /** The path of the Material Design icon to render. */
    iconPath?: string;
    /** The additional className to attach. */
    className?: string;
};

export const CardHead = ({ label, style = HeadStyle.Default, iconHash, iconPath, className }: Props) => {
    const classes = classNames(className, styles.head, styles[`style-${style}`]);

    const iconSize = style === HeadStyle.Title ? IconSize.Large : IconSize.Medium;
    const textSize = style === HeadStyle.Title ? TextSize.Title : TextSize.Large;

    return (
        <div className={classes}>
            <Icon className={styles.icon} hash={iconHash} path={iconPath} size={iconSize} />
            <Text className={styles.label} text={label} size={textSize} />
        </div>
    );
};

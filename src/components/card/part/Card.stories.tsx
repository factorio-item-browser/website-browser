import type { Meta, StoryObj } from "@storybook/react";
import { Text } from "../../text/Text.tsx";
import { Card } from "./Card.tsx";

const meta: Meta<typeof Card> = {
    title: "card/part/Card",
    component: Card,
};
export default meta;

type Story = StoryObj<typeof Card>;

const text = `Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla leo ex, lobortis quis lacus sit amet,
euismod auctor nisi. Praesent a leo ac lacus iaculis pulvinar. Ut nec enim hendrerit, varius tellus a, mollis nulla.
Phasellus rutrum orci nec vestibulum aliquam. Duis porta massa eget massa vulputate, ut pulvinar nibh lacinia. Cras 
mattis consectetur metus in sollicitudin. Nam id metus eget nunc vestibulum aliquet vestibulum in velit. 
Integer nisi magna, ultrices vitae accumsan id, gravida a purus.`;

export const Component: Story = {
    args: {
        spacing: false,
    },
    render: (args) => (
        <Card {...args}>
            <div style={{ padding: "8px" }}>
                <Text text={text} />
            </div>
        </Card>
    ),
};

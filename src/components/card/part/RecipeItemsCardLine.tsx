import classNames from "classnames";
import { TextStyle } from "../../../constants/text.ts";
import { Icon } from "../../icon/Icon.tsx";
import { Text } from "../../text/Text.tsx";
import styles from "./RecipeItemsCardLine.module.css";

type Props = {
    /** The label to display in the item line. */
    label: string;
    /** The amount of the item to display, already formatted. */
    amount: string;
    /** The hash of the stylesheet icon to render. */
    iconHash?: string;
    /** The path of the Material Design icon to render. */
    iconPath?: string;
    /** The additional className to attach. */
    className?: string;
};

export const RecipeItemsCardLine = ({ label, amount, iconHash, iconPath, className }: Props) => {
    const classes = classNames(className, styles.line);

    return (
        <div className={classes}>
            <Text className={styles.amount} text={amount} style={TextStyle.Bold} />
            <Icon className={styles.icon} hash={iconHash} path={iconPath} />
            <Text className={styles.label} text={label} style={TextStyle.Bold} nowrap />
        </div>
    );
};

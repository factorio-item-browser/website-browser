import classNames from "classnames";
import styles from "./CardSpacer.module.css";

type Props = {
    /** The additional className to attach. */
    className?: string;
};

export const CardSpacer = ({ className }: Props) => {
    const classes = classNames(className, styles.spacer);

    return <div className={classes} />;
};

import type { Meta, StoryObj } from "@storybook/react";
import { exampleRecipes } from "../../../.storybook/examples.ts";
import { CompactRecipeCard } from "./CompactRecipeCard.tsx";

const meta: Meta<typeof CompactRecipeCard> = {
    title: "card/CompactRecipeCard",
    component: CompactRecipeCard,
};
export default meta;

type Story = StoryObj<typeof CompactRecipeCard>;

export const Component: Story = {
    args: {
        recipe: exampleRecipes.advancedCircuit,
        spacing: false,
    },
};

import type { Meta, StoryObj } from "@storybook/react";
import { exampleMachines, exampleRecipes } from "../../../.storybook/examples.ts";
import { CardSpacer } from "./CardSpacer.tsx";
import { MachineCard } from "./MachineCard.tsx";

const meta: Meta<typeof MachineCard> = {
    title: "card/MachineCard",
    component: MachineCard,
};
export default meta;

type Story = StoryObj<typeof MachineCard>;

export const Component: Story = {
    args: {
        machine: exampleMachines.assemblingMachine3,
        recipe: exampleRecipes.copperCable,
    },
};

export const MiningRecipe: Story = {
    name: "Mining recipe",
    args: {
        machine: exampleMachines.pumpjack,
        recipe: exampleRecipes.crudeOil,
    },
};

export const Character: Story = {
    render: () => (
        <>
            <MachineCard machine={exampleMachines.character} recipe={exampleRecipes.copperCable} />
            <CardSpacer />
            <MachineCard machine={exampleMachines.character} recipe={exampleRecipes.crudeOil} />
        </>
    ),
};

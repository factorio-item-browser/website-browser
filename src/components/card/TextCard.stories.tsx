import { mdiText } from "@mdi/js";
import type { Meta, StoryObj } from "@storybook/react";
import { TextCard } from "./TextCard.tsx";

const meta: Meta<typeof TextCard> = {
    title: "card/TextCard",
    component: TextCard,
};
export default meta;

type Story = StoryObj<typeof TextCard>;

export const Component: Story = {
    args: {
        text: `Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec venenatis dictum sem at finibus. Sed 
malesuada vel nisl eu consectetur. Vestibulum eu hendrerit lacus. Aliquam nunc mi, tincidunt sit amet feugiat sed, 
finibus id ligula.`,
        iconPath: mdiText,
    },
};

import classNames from "classnames";
import { IconSize } from "../../constants/icon.ts";
import { TextStyle } from "../../constants/text.ts";
import { Card } from "../card/part/Card.tsx";
import { Icon } from "../icon/Icon.tsx";
import { Text } from "../text/Text.tsx";
import styles from "./SidebarButton.module.css";

type Props = {
    /** The label of the button. */
    label: string;
    /** The hash of the stylesheet icon to render. */
    iconHash?: string;
    /** The path of the Material Design icon to render. */
    iconPath?: string;
    /** The additional className to attach. */
    className?: string;
};

export const SidebarButton = ({ label, iconHash, iconPath, className }: Props) => {
    const classes = classNames(styles.card, className);

    return (
        <Card className={classes} spacing>
            <Icon className={styles.icon} hash={iconHash} path={iconPath} size={IconSize.Medium} />
            <Text className={styles.label} text={label} style={TextStyle.Bold} nowrap />
        </Card>
    );
};

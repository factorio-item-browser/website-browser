import { mdiAlert } from "@mdi/js";
import type { Meta, StoryObj } from "@storybook/react";
import { SidebarButton } from "./SidebarButton.tsx";

const meta: Meta<typeof SidebarButton> = {
    title: "button/SidebarButton",
    component: SidebarButton,
};
export default meta;

type Story = StoryObj<typeof SidebarButton>;

export const Component: Story = {
    args: {
        label: "Lorem ipsum dolor sit amet",
        iconHash: "item-electronic-circuit",
    },
};

export const Overflow: Story = {
    name: "Overflowing content",
    args: {
        label: "Lorem ipsum dolor sit amet, consectetur adipiscing elit",
        iconPath: mdiAlert,
    },
};

import classNames from "classnames";
import { useTranslation } from "react-i18next";
import { IconSize } from "../../constants/icon.ts";
import { TextSize, TextStyle } from "../../constants/text.ts";
import { Entity } from "../../model/entity/entity.ts";
import { Card } from "../card/part/Card.tsx";
import { FavoriteIcon } from "../icon/FavoriteIcon.tsx";
import { Icon } from "../icon/Icon.tsx";
import { Text } from "../text/Text.tsx";
import styles from "./SidebarEntityButton.module.css";

type Props = {
    /** The entity to display the button for. */
    entity: Entity;
    /** Whether the entity is currently in the list of favorites. */
    favorite?: boolean;
    /** The handler for when the favorite icon is clicked. */
    onFavoriteClick?: (entity: Entity) => void | Promise<void>;
    /** The additional className to attach. */
    className?: string;
};

export const SidebarEntityButton = ({ entity, favorite = false, onFavoriteClick, className }: Props) => {
    const { t } = useTranslation("common");
    const handleFavoriteClick = () => {
        onFavoriteClick && onFavoriteClick(entity);
    };

    const classes = classNames(styles.card, className);

    return (
        <Card className={classes} spacing>
            <Icon className={styles.icon} hash={entity.iconHash} size={IconSize.Medium} />
            <Text
                className={styles.type}
                text={t(`common:entity-type.${entity.type}`)}
                size={TextSize.Small}
                style={TextStyle.Uppercase}
                nowrap
            />
            <Text className={styles.label} text={entity.label} style={TextStyle.Bold} nowrap />
            <FavoriteIcon className={styles.star} isFavorite={favorite} onClick={handleFavoriteClick} />
        </Card>
    );
};

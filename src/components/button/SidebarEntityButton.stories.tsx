import type { Meta, StoryObj } from "@storybook/react";
import { exampleItems } from "../../../.storybook/examples.ts";
import { SidebarEntityButton } from "./SidebarEntityButton.tsx";

const meta: Meta<typeof SidebarEntityButton> = {
    title: "button/SidebarEntityButton",
    component: SidebarEntityButton,
};
export default meta;

type Story = StoryObj<typeof SidebarEntityButton>;

export const Component: Story = {
    args: {
        entity: exampleItems.electronicCircuit,
        favorite: false,
    },
};

export const Overflow: Story = {
    name: "Overflowing content",
    args: {
        entity: {
            ...exampleItems.crudeOil,
            label: "Lorem ipsum dolor sit amet, consectetur adipiscing elit",
        },
        favorite: false,
    },
};

import type { Meta, StoryObj } from "@storybook/react";
import { TextSize, TextStyle } from "../../constants/text.ts";
import { Text } from "./Text.tsx";

const meta: Meta<typeof Text> = {
    title: "text/Text",
    component: Text,
    parameters: {
        backgrounds: {
            default: "none",
        },
    },
};
export default meta;

type Story = StoryObj<typeof Text>;

export const Component: Story = {
    args: {
        text: "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec molestie eros ut lorem bibendum lobortis. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam ullamcorper pretium metus at rhoncus. Aliquam in eleifend orci.",
        size: TextSize.Medium,
        style: TextStyle.Default,
        nowrap: false,
    },
};

export const Sizes: Story = {
    name: "Different sizes",
    render: () => (
        <>
            <div>
                <Text text={"TextSize.Small"} size={TextSize.Small} />
            </div>
            <div>
                <Text text={"TextSize.Medium"} size={TextSize.Medium} />
            </div>
            <div>
                <Text text={"TextSize.Large"} size={TextSize.Large} />
            </div>
            <div>
                <Text text={"TextSize.Headline"} size={TextSize.Headline} />
            </div>
            <div>
                <Text text={"TextSize.Title"} size={TextSize.Title} />
            </div>
        </>
    ),
};

export const Styles: Story = {
    name: "Different styles",
    render: () => (
        <>
            <div>
                <Text text={"TextStyle.Default"} style={TextStyle.Default} />
            </div>
            <div>
                <Text text={"TextStyle.Bold"} style={TextStyle.Bold} />
            </div>
            <div>
                <Text text={"TextStyle.Monospace"} style={TextStyle.Monospace} />
            </div>
            <div>
                <Text text={"TextStyle.Uppercase"} style={TextStyle.Uppercase} />
            </div>
            <div>
                <Text text={"TextStyle.White"} style={TextStyle.White} />
            </div>
        </>
    ),
};

export const Nowrap: Story = {
    name: "Disabled line wrapping",
    render: () => (
        <>
            <Text
                text={
                    "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec molestie eros ut lorem bibendum lobortis. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam ullamcorper pretium metus at rhoncus. Aliquam in eleifend orci. Nunc mi risus, malesuada sed odio non, suscipit facilisis sem."
                }
                nowrap
            />
        </>
    ),
};

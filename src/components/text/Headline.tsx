import classNames from "classnames";
import { HeadlineStyle } from "../../constants/text.ts";
import styles from "./Headline.module.css";

type Props = {
    /** The label of the headline. */
    label: string;
    /** The style of the headline. */
    style?: HeadlineStyle;
    /** The additional className to attach. */
    className?: string;
};

export const Headline = ({ label, style = HeadlineStyle.Default, className }: Props) => {
    const classes = classNames(className, styles.headline, styles[`style-${style}`]);
    const HeadlineTag = style === HeadlineStyle.Sidebar ? "h3" : "h2";

    return <HeadlineTag className={classes}>{label}</HeadlineTag>;
};

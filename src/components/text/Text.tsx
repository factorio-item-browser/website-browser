import classNames from "classnames";
import { forwardRef } from "react";
import { TextSize, TextStyle } from "../../constants/text.ts";
import styles from "./Text.module.css";

type Props = {
    /** The text to display. */
    text: string;
    /** The size of the text. */
    size?: TextSize;
    /** The style of the text. */
    style?: TextStyle;
    /** Whether the text should not be wrapped, and instead cut off at the end. */
    nowrap?: boolean;
    /** The additional className to attach. */
    className?: string;
};

export const Text = forwardRef<HTMLSpanElement, Props>(
    ({ text, size = TextSize.Medium, style = TextStyle.Default, nowrap = false, className }: Props, ref) => {
        const classes = classNames(className, styles.text, styles[`size-${size}`], styles[`style-${style}`], {
            [styles.nowrap]: nowrap,
        });

        return (
            <span className={classes} ref={ref}>
                {text}
            </span>
        );
    },
);

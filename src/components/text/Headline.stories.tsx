import type { Meta, StoryObj } from "@storybook/react";
import { HeadlineStyle } from "../../constants/text.ts";
import { Headline } from "./Headline.tsx";

const meta: Meta<typeof Headline> = {
    title: "text/Headline",
    component: Headline,
};
export default meta;

type Story = StoryObj<typeof Headline>;

export const Component: Story = {
    args: {
        label: "Lorem ipsum dolor sit amet",
    },
};

export const Styles: Story = {
    name: "Different styles",
    render: () => (
        <>
            <div>
                <Headline label={"Headline.Default"} style={HeadlineStyle.Default} />
            </div>
            <div>
                <Headline label={"Headline.Sidebar"} style={HeadlineStyle.Sidebar} />
            </div>
        </>
    ),
};

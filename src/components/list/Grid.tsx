import classNames from "classnames";
import { CSSProperties, ReactNode } from "react";
import styles from "./Grid.module.css";

type Props = {
    /** The number of columns to use in the grid. */
    columns: number;
    /** The elements to be arranged in a grid. */
    children: ReactNode;
    /** The additional className to attach. */
    className?: string;
};

export const Grid = ({ columns, className, children }: Props) => {
    const classes = classNames(className, styles.grid);
    const style = {
        ["--num-columns"]: columns,
    } as CSSProperties;

    return (
        <div className={classes} style={style}>
            {children}
        </div>
    );
};

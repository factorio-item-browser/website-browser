import type { Meta, StoryObj } from "@storybook/react";
import { ComponentProps } from "react";
import { exampleMods } from "../../../.storybook/examples.ts";
import { ModCard } from "../card/ModCard.tsx";
import { Grid } from "./Grid.tsx";

type ExtendedArgs = ComponentProps<typeof Grid> & {
    count: number;
};

const meta: Meta<ExtendedArgs> = {
    title: "list/Grid",
    component: Grid,
    argTypes: {
        count: {
            name: "count",
            type: { name: "number", required: true },
            description: "The number of mods to render for the preview. (Not used in the actual component.)",
            defaultValue: 16,
            table: {
                category: "Storybook preview",
            },
        },
    },
    render: ({ count, ...props }) => (
        <Grid {...props}>
            {[...Array(count).keys()].map((idx) => (
                <ModCard key={idx} mod={exampleMods.base} />
            ))}
        </Grid>
    ),
};
export default meta;

type Story = StoryObj<ExtendedArgs>;

export const Component: Story = {
    args: {
        count: 16,
        columns: 4,
    },
};

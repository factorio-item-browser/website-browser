import { mdiClockOutline, mdiStar } from "@mdi/js";
import { ReactSortable } from "react-sortablejs";
import { SectionStyle } from "../../constants/section.ts";
import { Entity } from "../../model/entity/entity.ts";
import { SidebarEntityButton } from "../button/SidebarEntityButton.tsx";
import { EntityLink } from "../link/EntityLink.tsx";
import { Section } from "../section/Section.tsx";
import styles from "./SidebarEntityList.module.css";

type Props = {
    /** The headline of the entity list. */
    headline: string;
    /** The entities to show in the list. */
    entities: Entity[];
    /** The handler for when the favorite icon of an entity is clicked. */
    onFavoriteClick?: (entity: Entity) => void | Promise<void>;
    /** The handler for when the order of the entities have been changed by the user. */
    onOrderChange?: (entities: Entity[]) => void | Promise<void>;
    /** Whether the entities are the favorites. */
    favorites?: boolean;
    /** The additional className to attach. */
    className?: string;
};

type Item = {
    id: string;
    entity: Entity;
};

function createItems(entities: Entity[]): Item[] {
    return entities.map((entity) => {
        return {
            id: entity.key,
            entity: entity,
        };
    });
}

export const SidebarEntityList = ({
    headline,
    entities,
    onFavoriteClick,
    onOrderChange,
    favorites = false,
    className,
}: Props) => {
    if (entities.length === 0) {
        return null;
    }

    const items = createItems(entities);
    let orderedEntities: Entity[] = [];
    const setList = (items: Item[]) => {
        orderedEntities = items.map((item) => item.entity);
    };
    const handleSort = () => {
        onOrderChange && onOrderChange(orderedEntities);
    };

    return (
        <Section
            className={className}
            headline={headline}
            headIconPath={favorites ? mdiStar : mdiClockOutline}
            style={SectionStyle.Sidebar}
            spacing
            collapsable
        >
            <ReactSortable
                list={items}
                setList={setList}
                onSort={handleSort}
                group={{
                    name: "sidebar-entities",
                    pull: !favorites,
                    put: favorites,
                }}
                sort={favorites}
                draggable={`.${styles.entity}`}
                animation={100}
            >
                {entities.map((entity) => (
                    <EntityLink key={entity.key} entity={entity} className={styles.entity} box hover>
                        <SidebarEntityButton entity={entity} favorite={favorites} onFavoriteClick={onFavoriteClick} />
                    </EntityLink>
                ))}
            </ReactSortable>
        </Section>
    );
};

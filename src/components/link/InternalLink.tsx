import classNames from "classnames";
import { useAtomValue } from "jotai";
import { ReactNode } from "react";
import { Link } from "react-router-dom";
import { combinationIdAtom } from "../../state/combination.ts";
import styles from "./InternalLink.module.css";

type Props = {
    /** The route to link to. */
    route: string;
    /** Whether the link is a box link, with an additional background animation. */
    box?: boolean;
    /** Whether to add a hover effect for the link, changing the background color. */
    hover?: boolean;
    /** The content of the link. */
    children: ReactNode;
    /** The additional className to attach. */
    className?: string;
};

export const InternalLink = ({ route, box, hover, className, children }: Props) => {
    const combinationId = useAtomValue(combinationIdAtom);
    if (!route.startsWith("/")) {
        route = `/${combinationId}/${route}`;
    }

    const classes = classNames(className, styles.link, {
        [styles.box]: box,
        [styles.hover]: hover,
    });

    return (
        <Link className={classes} to={route}>
            {children}
        </Link>
    );
};

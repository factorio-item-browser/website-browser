import type { Meta, StoryObj } from "@storybook/react";
import { ExternalLink } from "./ExternalLink.tsx";

const meta: Meta<typeof ExternalLink> = {
    title: "link/ExternalLink",
    component: ExternalLink,
    parameters: {
        backgrounds: {
            default: "none",
        },
    },
};
export default meta;

type Story = StoryObj<typeof ExternalLink>;

export const Component: Story = {
    args: {
        url: "https://www.factorio.com/",
    },
    render: (args) => <ExternalLink {...args}>factorio.com</ExternalLink>,
};

import classNames from "classnames";
import { ReactNode } from "react";
import styles from "./ExternalLink.module.css";

type Props = {
    /** The url to link to. */
    url: string;
    /** Whether the link is a box link, with an additional background animation. */
    box?: boolean;
    /** Whether to add a hover effect for the link, changing the background color. */
    hover?: boolean;
    /** The content of the link. */
    children: ReactNode;
    /** The additional className to attach. */
    className?: string;
};

export const ExternalLink = ({ url, box, hover, className, children }: Props) => {
    const classes = classNames(className, styles.link, {
        [styles.box]: box,
        [styles.hover]: hover,
    });

    return (
        <a className={classes} href={url} target="_blank" rel="noopener noreferrer nofollow">
            {children}
        </a>
    );
};

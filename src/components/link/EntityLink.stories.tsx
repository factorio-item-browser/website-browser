import type { Meta, StoryObj } from "@storybook/react";
import { exampleItems, exampleMods } from "../../../.storybook/examples.ts";
import { Text } from "../text/Text.tsx";
import { EntityLink } from "./EntityLink.tsx";

const meta: Meta<typeof EntityLink> = {
    title: "link/EntityLink",
    component: EntityLink,
    parameters: {
        backgrounds: {
            default: "none",
        },
    },
};
export default meta;

type Story = StoryObj<typeof EntityLink>;

export const Component: Story = {
    args: {
        entity: exampleItems.electronicCircuit,
    },
    render: (args) => (
        <EntityLink {...args}>
            <Text text={"Electronic circuit"} />
        </EntityLink>
    ),
};

export const ModLinking: Story = {
    args: {
        entity: exampleMods.spaceExploration,
    },
    render: (args) => (
        <EntityLink {...args}>
            <Text text={"Mod: Space Exploration"} />
        </EntityLink>
    ),
};

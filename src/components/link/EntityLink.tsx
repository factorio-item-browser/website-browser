import { ReactNode } from "react";
import { ModType, RecipeType } from "../../constants/entity.ts";
import { Entity } from "../../model/entity/entity.ts";
import { ExternalLink } from "./ExternalLink.tsx";
import { InternalLink } from "./InternalLink.tsx";

type Props = {
    /** The entity to link to. */
    entity: Entity;
    /** Whether the link is a box link, with an additional background animation. */
    box?: boolean;
    /** Whether to add a hover effect for the link, changing the background color. */
    hover?: boolean;
    /** The content of the link. */
    children: ReactNode;
    /** The additional className to attach. */
    className?: string;
};

export const EntityLink = ({ entity, box, hover, className, children }: Props) => {
    if (entity.type === ModType.Mod) {
        let link = `https://mods.factorio.com/mod/${entity.name}`;
        if (entity.name === "base") {
            link = "https://www.factorio.com/";
        }

        return (
            <ExternalLink className={className} url={link} box={box} hover={hover}>
                {children}
            </ExternalLink>
        );
    }

    let route = `${entity.type}/${entity.name}`;
    if (
        entity.type === RecipeType.Crafting ||
        entity.type === RecipeType.Mining ||
        entity.type === RecipeType.RocketLaunch
    ) {
        route = `recipe/${route}`;
    }

    return (
        <InternalLink className={className} route={route} box={box} hover={hover}>
            {children}
        </InternalLink>
    );
};

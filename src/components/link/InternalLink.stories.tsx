import type { Meta, StoryObj } from "@storybook/react";
import { Text } from "../text/Text.tsx";
import { InternalLink } from "./InternalLink.tsx";

const meta: Meta<typeof InternalLink> = {
    title: "link/InternalLink",
    component: InternalLink,
    parameters: {
        backgrounds: {
            default: "none",
        },
    },
};
export default meta;

type Story = StoryObj<typeof InternalLink>;

export const Component: Story = {
    args: {
        route: "/item/electronic-circuit",
    },
    render: (args) => (
        <InternalLink {...args}>
            <Text text={"Item: Electronic circuit"} />
        </InternalLink>
    ),
};

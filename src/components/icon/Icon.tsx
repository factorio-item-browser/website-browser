import MdiIcon from "@mdi/react";
import classNames from "classnames";
import { IconSize } from "../../constants/icon.ts";
import styles from "./Icon.module.css";

type Props = {
    /** The hash of the stylesheet icon to render. */
    hash?: string;
    /** The path of the Material Design icon to render. */
    path?: string;
    /** The size of the icon. */
    size?: IconSize;
    /** The additional className to attach. */
    className?: string;
};

export const Icon = ({ hash, path, size = IconSize.Medium, className }: Props) => {
    const classes = classNames(className, styles.icon, styles[size], {
        [`icon-${hash}`]: hash && !path,
    });

    if (path) {
        return <MdiIcon path={path} className={classes} />;
    }

    return <div className={classes} />;
};

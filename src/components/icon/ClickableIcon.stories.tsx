import type { Meta, StoryObj } from "@storybook/react";
import { IconSize } from "../../constants/icon.ts";
import { ClickableIcon } from "./ClickableIcon.tsx";

const meta: Meta<typeof ClickableIcon> = {
    title: "icon/ClickableIcon",
    component: ClickableIcon,
    argTypes: {
        onClick: {
            action: "clicked",
        },
        onMouseEnter: {
            action: "mouse entered",
        },
        onMouseLeave: {
            action: "mouse left",
        },
    },
};
export default meta;

type Story = StoryObj<typeof ClickableIcon>;

export const Component: Story = {
    args: {
        hash: "item-electronic-circuit",
        size: IconSize.Medium,
    },
};

import { mdiStar, mdiStarMinus, mdiStarOutline, mdiStarPlusOutline } from "@mdi/js";
import { useState } from "react";
import { useTranslation } from "react-i18next";
import { ClickableIcon } from "./ClickableIcon.tsx";

type Props = {
    /** Whether the target is currently a favorite, i.e. the click action is removing it from the favorites. */
    isFavorite?: boolean;
    /** The handler for when the user clicked on the star. */
    onClick?: () => void | Promise<void>;
    /** The additional className to attach. */
    className?: string;
};

export const FavoriteIcon = ({ isFavorite, onClick, className }: Props) => {
    const [hover, setHover] = useState(false);
    const { t } = useTranslation("components");

    const handleMouseEnter = () => setHover(true);
    const handleMouseLeave = () => setHover(false);

    let iconPath = hover ? mdiStarPlusOutline : mdiStarOutline;
    if (isFavorite) {
        iconPath = hover ? mdiStarMinus : mdiStar;
    }

    const label = t(`components:icon.favorite-icon.${isFavorite ? "remove-favorite" : "add-favorite"}`);

    return (
        <ClickableIcon
            className={className}
            path={iconPath}
            title={label}
            onClick={onClick}
            onMouseEnter={handleMouseEnter}
            onMouseLeave={handleMouseLeave}
        />
    );
};

import classNames from "classnames";
import { MouseEvent } from "react";
import { IconSize } from "../../constants/icon.ts";
import styles from "./ClickableIcon.module.css";
import { Icon } from "./Icon.tsx";

type Props = {
    /** The hash of the stylesheet icon to render. */
    hash?: string;
    /** The path of the Material Design icon to render. */
    path?: string;
    /** The size of the icon. */
    size?: IconSize;
    /** The title to use on the icon when hovering with the mouse. */
    title?: string;
    /** The handler for when the user clicked on the îcon. */
    onClick?: () => void | Promise<void>;
    /** The handler for when the mouse entered the icon. */
    onMouseEnter?: () => void | Promise<void>;
    /** The handler for when the mouse left the icon. */
    onMouseLeave?: () => void | Promise<void>;
    /** The additional className to attach. */
    className?: string;
};

function createHandler(callback?: () => void | Promise<void>) {
    return (event: MouseEvent<HTMLDivElement>) => {
        event.preventDefault();
        event.stopPropagation();

        callback && callback();
    };
}

export const ClickableIcon = ({
    hash,
    path,
    size = IconSize.Medium,
    title,
    onClick,
    onMouseEnter,
    onMouseLeave,
    className,
}: Props) => {
    const classes = classNames(className, styles.icon);

    const handleClick = createHandler(onClick);
    const handleMouseEnter = createHandler(onMouseEnter);
    const handleMouseLeave = createHandler(onMouseLeave);

    return (
        <div
            className={classes}
            title={title}
            onClick={handleClick}
            onMouseEnter={handleMouseEnter}
            onMouseLeave={handleMouseLeave}
        >
            <Icon hash={hash} path={path} size={size} />
        </div>
    );
};

import { mdiCog, mdiMagnify, mdiMenu, mdiSpeedometer, mdiStarOutline } from "@mdi/js";
import type { Meta, StoryObj } from "@storybook/react";
import { IconSize } from "../../constants/icon.ts";
import { Icon } from "./Icon";

const meta: Meta<typeof Icon> = {
    title: "icon/Icon",
    component: Icon,
    parameters: {
        backgrounds: {
            default: "none",
        },
    },
};
export default meta;

type Story = StoryObj<typeof Icon>;

export const Component: Story = {
    args: {
        hash: "item-electronic-circuit",
        size: IconSize.Medium,
    },
};

export const Sizes: Story = {
    name: "Different sizes",
    render: () => (
        <>
            <Icon hash="item-iron-plate" size={IconSize.Tiny} />
            <Icon hash="item-copper-cable" size={IconSize.Small} />
            <Icon hash="item-electronic-circuit" size={IconSize.Medium} />
            <Icon hash="fluid-crude-oil" size={IconSize.Large} />
            <Icon hash="mod-base" size={IconSize.Huge} />
        </>
    ),
};

export const MaterialDesign: Story = {
    name: "Material Design icons",
    render: () => (
        <>
            <Icon path={mdiSpeedometer} size={IconSize.Tiny} />
            <Icon path={mdiMenu} size={IconSize.Small} />
            <Icon path={mdiStarOutline} />
            <Icon path={mdiMagnify} size={IconSize.Large} />
            <Icon path={mdiCog} size={IconSize.Huge} />
        </>
    ),
};

import type { Meta, StoryObj } from "@storybook/react";
import { FavoriteIcon } from "./FavoriteIcon.tsx";

const meta: Meta<typeof FavoriteIcon> = {
    title: "icon/FavoriteIcon",
    component: FavoriteIcon,
    argTypes: {
        onClick: {
            action: "clicked",
        },
    },
    parameters: {
        backgrounds: {
            default: "none",
        },
    },
};
export default meta;

type Story = StoryObj<typeof FavoriteIcon>;

export const Component: Story = {
    args: {
        isFavorite: false,
    },
};

export const Active: Story = {
    name: "Active state",
    args: {
        isFavorite: true,
    },
};

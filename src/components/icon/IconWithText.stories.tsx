import type { Meta, StoryObj } from "@storybook/react";
import { TextSize } from "../../constants/text.ts";
import { Text } from "../text/Text.tsx";
import { IconWithText } from "./IconWithText.tsx";

const meta: Meta<typeof IconWithText> = {
    title: "icon/IconWithText",
    component: IconWithText,
    parameters: {
        backgrounds: {
            default: "none",
        },
    },
};
export default meta;

type Story = StoryObj<typeof IconWithText>;

export const Component: Story = {
    args: {
        text: "42x",
        hash: "item-electronic-circuit",
        top: false,
        bottom: false,
        left: false,
        right: false,
    },
};

export const Positioning: Story = {
    render: () => (
        <div
            style={{
                display: "grid",
                gridTemplateColumns: "repeat(4, min-content)",
                gridGap: "1rem",
                alignItems: "center",
                textAlign: "center",
            }}
        >
            <Text text={""} />
            <Text text={"left"} size={TextSize.Large} />
            <Text text={"default"} size={TextSize.Large} />
            <Text text={"right"} size={TextSize.Large} />
            <Text text={"top"} size={TextSize.Large} />
            <IconWithText text={"42x"} hash="item-electronic-circuit" top left />
            <IconWithText text={"42x"} hash="item-electronic-circuit" top />
            <IconWithText text={"42x"} hash="item-electronic-circuit" top right />
            <Text text={"default"} size={TextSize.Large} />
            <IconWithText text={"42x"} hash="item-electronic-circuit" left />
            <IconWithText text={"42x"} hash="item-electronic-circuit" />
            <IconWithText text={"42x"} hash="item-electronic-circuit" right />
            <Text text={"bottom"} size={TextSize.Large} />
            <IconWithText text={"42x"} hash="item-electronic-circuit" bottom left />
            <IconWithText text={"42x"} hash="item-electronic-circuit" bottom />
            <IconWithText text={"42x"} hash="item-electronic-circuit" bottom right />
        </div>
    ),
};

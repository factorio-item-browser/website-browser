import classNames from "classnames";
import { IconSize } from "../../constants/icon.ts";
import { TextStyle } from "../../constants/text.ts";
import { Text } from "../text/Text.tsx";
import { Icon } from "./Icon.tsx";
import styles from "./IconWithText.module.css";

type Props = {
    /** The text to attach. */
    text: string;
    /** The hash of the stylesheet icon to render. */
    hash?: string;
    /** The path of the Material Design icon to render. */
    path?: string;
    /** Whether to align the attached text to the bottom. */
    bottom?: boolean;
    /** Whether to align the attached text to the top. */
    top?: boolean;
    /** Whether to align the attached text to the left. */
    left?: boolean;
    /** Whether to align the attached text to the right. */
    right?: boolean;
    /** The additional className to attach. */
    className?: string;
    /** The additional className to attach to the icon. */
    iconClassName?: string;
    /** The additional className to attach to the text. */
    textClassName?: string;
};

export const IconWithText = ({
    text,
    hash,
    path,
    bottom = false,
    top = false,
    left = false,
    right = false,
    className,
    iconClassName,
    textClassName,
}: Props) => {
    const classes = classNames(className, styles.wrapper);
    const textClasses = classNames(textClassName, styles.text, {
        [styles.top]: top,
        [styles.left]: left,
        [styles.right]: right,
        [styles.bottom]: bottom,
    });

    return (
        <div className={classes}>
            <Icon className={iconClassName} hash={hash} path={path} size={IconSize.Medium} />
            <Text className={textClasses} text={text} style={TextStyle.White} />
        </div>
    );
};

import { useAtomValue } from "jotai";
import { breakpointAtom } from "../../state/breakpoint.ts";
import { themeAtom } from "../../state/theme.ts";

export const AttributeInjector = () => {
    const breakpoint = useAtomValue(breakpointAtom);
    const theme = useAtomValue(themeAtom);

    document.documentElement.setAttribute("data-breakpoint", breakpoint);
    document.documentElement.setAttribute("data-theme", theme);

    return null;
};

import { useEffect } from "react";

type Props = {
    /** The id of the combination to load the style for. */
    combinationId: string;
};

export const CombinationStyleLoader = ({ combinationId }: Props) => {
    useEffect(() => {
        const link = document.createElement("link");
        link.type = "text/css";
        link.rel = "stylesheet";
        link.href = `${import.meta.env.VITE_FIB_BASE_URL}/stylesheet/${combinationId}.css?v=45`;
        // link.addEventListener("load", () => {
        //     console.log("stylesheet has loaded!");
        // });

        const head = document.getElementsByTagName("head")[0];
        head.appendChild(link);

        return () => {
            head.removeChild(link);
        };
    }, [combinationId]);

    return null;
};

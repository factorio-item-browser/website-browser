import { createBrowserRouter, Params } from "react-router-dom";
import { ItemType } from "./constants/entity.ts";
import { ErrorPage } from "./pages/ErrorPage.tsx";
import { IndexPage } from "./pages/IndexPage.tsx";
import { ItemDetailsPage } from "./pages/ItemDetailsPage.tsx";
import { ItemListPage } from "./pages/ItemListPage.tsx";
import { MachineDetailsPage } from "./pages/MachineDetailsPage.tsx";
import { RecipeDetailsPage } from "./pages/RecipeDetailsPage.tsx";
import { RootPage } from "./pages/RootPage.tsx";
import { combinationIdAtom } from "./state/combination.ts";
import { store } from "./state/store.ts";

const layoutLoader = ({ params }: { params: Params<"combinationId"> }): null => {
    store.set(combinationIdAtom, params.combinationId!);
    return null;
};

export const router = createBrowserRouter([
    {
        path: "/:combinationId",
        element: <RootPage />,
        loader: layoutLoader,
        children: [
            {
                path: "",
                element: <IndexPage />,
                loader: IndexPage.loader,
                errorElement: <ErrorPage />,
            },
            {
                path: "items",
                element: <ItemListPage />,
                loader: ItemListPage.loader,
                errorElement: <ErrorPage />,
            },
            {
                path: "machine/:name",
                element: <MachineDetailsPage />,
                loader: MachineDetailsPage.loader,
                errorElement: <ErrorPage />,
            },
            {
                path: "recipe/:type/:name",
                element: <RecipeDetailsPage />,
                loader: RecipeDetailsPage.loader,
                errorElement: <ErrorPage />,
            },
            // Item routes
            {
                path: "item/:name",
                element: <ItemDetailsPage />,
                loader: ItemDetailsPage.createLoader(ItemType.Item),
                errorElement: <ErrorPage />,
            },
            {
                path: "fluid/:name",
                element: <ItemDetailsPage />,
                loader: ItemDetailsPage.createLoader(ItemType.Fluid),
                errorElement: <ErrorPage />,
            },
            {
                path: "resource/:name",
                element: <ItemDetailsPage />,
                loader: ItemDetailsPage.createLoader(ItemType.Resource),
                errorElement: <ErrorPage />,
            },
        ],
    },
]);

import { useAtomValue } from "jotai/index";
import { matchRoutes, Outlet, ScrollRestoration, useLocation } from "react-router-dom";
import { AttributeInjector } from "../components/feature/AttributeInjector.tsx";
import { CombinationStyleLoader } from "../components/feature/CombinationStyleLoader.tsx";
import { Layout } from "../components/layout/Layout.tsx";
import { combinationIdAtom } from "../state/combination.ts";

export const RootPage = () => {
    const combinationId = useAtomValue(combinationIdAtom);

    const location = useLocation();
    const matches = matchRoutes([{ path: "/:combinationId" }], location);
    const isIndex = !!(matches && matches.length > 0);

    return (
        <>
            <AttributeInjector />
            <CombinationStyleLoader combinationId={combinationId} />
            <ScrollRestoration />

            <Layout index={isIndex}>
                <Outlet />
            </Layout>
        </>
    );
};

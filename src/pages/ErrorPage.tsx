import { useTranslation } from "react-i18next";
import { useRouteError } from "react-router-dom";
import { ErrorCard } from "../components/card/ErrorCard.tsx";
import { AbstractError } from "../errors/abstract.ts";

export const ErrorPage = () => {
    const error = useRouteError();
    const { t } = useTranslation(["common", "errors"]);

    let title: string;
    let message: string;
    if (error instanceof AbstractError) {
        title = error.getTitle(t);
        message = error.getMessage(t);
    } else {
        title = t("errors:unexpected.title");
        message = t("errors:unexpected.message");
    }

    console.log(error);

    return <ErrorCard title={title} message={message} />;
};

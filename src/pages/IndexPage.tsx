import { useAtomValue } from "jotai";
import { useTranslation } from "react-i18next";
import { useLoaderData } from "react-router-dom";
import { EntityGridCard } from "../components/card/EntityGridCard.tsx";
import { ModCard } from "../components/card/ModCard.tsx";
import { Grid } from "../components/list/Grid.tsx";
import { Headline } from "../components/text/Headline.tsx";
import { Breakpoint } from "../constants/breakpoint.ts";
import { useDefaultDocumentTitle } from "../hooks/document.ts";
import { Item } from "../model/entity/item.ts";
import { Mod } from "../model/entity/mod.ts";
import { breakpointAtom } from "../state/breakpoint.ts";
import { repositoryAtom } from "../state/combination.ts";
import { optionsAtom } from "../state/options.ts";
import { store } from "../state/store.ts";

const gridColumns = {
    [Breakpoint.Small]: 1,
    [Breakpoint.Medium]: 2,
    [Breakpoint.Large]: 3,
    [Breakpoint.Huge]: 4,
};

type Data = {
    mods: Mod[];
    randomItems: Item[];
};

export const IndexPage = () => {
    const { t } = useTranslation("pages");
    const data = useLoaderData() as Data;
    const breakpoint = useAtomValue(breakpointAtom);

    useDefaultDocumentTitle();

    return (
        <>
            <EntityGridCard entities={data.randomItems} />

            <section>
                <Headline label={t("pages:index.mod-list.headline", { count: data.mods.length })} />
                <Grid columns={gridColumns[breakpoint]}>
                    {data.mods.map((mod) => (
                        <ModCard key={mod.key} mod={mod} />
                    ))}
                </Grid>
            </section>
        </>
    );
};

IndexPage.loader = async (): Promise<Data> => {
    const repository = store.get(repositoryAtom);
    const options = store.get(optionsAtom);

    const [mods, items] = await Promise.all([
        repository.mods.findAll(options),
        repository.items.findRandom(48, options),
    ]);

    return {
        mods: mods,
        randomItems: items,
    };
};

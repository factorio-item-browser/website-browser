import { useTranslation } from "react-i18next";
import { useLoaderData } from "react-router-dom";
import { EntityGridCard } from "../components/card/EntityGridCard.tsx";
import { Headline } from "../components/text/Headline.tsx";
import { useExtendedDocumentTitle } from "../hooks/document.ts";
import { usePagination } from "../hooks/pagination.ts";
import { Item } from "../model/entity/item.ts";
import { repositoryAtom } from "../state/combination.ts";
import { optionsAtom } from "../state/options.ts";
import { store } from "../state/store.ts";

type Data = {
    items: Item[];
};

export const ItemListPage = () => {
    const { t } = useTranslation("pages");
    const data = useLoaderData() as Data;

    useExtendedDocumentTitle(t("pages:item-list.title"));

    const [paginatedItems, hasNextPage, handleNextPage] = usePagination(data.items, 256);

    return (
        <section>
            <Headline label={t("pages:item-list.headline", { count: data.items.length })} />
            <EntityGridCard entities={paginatedItems} onNextPage={hasNextPage ? handleNextPage : undefined} />
        </section>
    );
};

ItemListPage.loader = async (): Promise<Data> => {
    const repository = store.get(repositoryAtom);
    const options = store.get(optionsAtom);

    const items = await repository.items.findAll(options);

    return {
        items: items,
    };
};

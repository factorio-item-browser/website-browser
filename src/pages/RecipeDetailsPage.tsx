import { mdiCogs } from "@mdi/js";
import { useAtomValue } from "jotai";
import { useTranslation } from "react-i18next";
import { Params, useLoaderData } from "react-router-dom";
import { CardSpacer } from "../components/card/CardSpacer.tsx";
import { MachineCard } from "../components/card/MachineCard.tsx";
import { Grid } from "../components/list/Grid.tsx";
import { RecipeDetails } from "../components/recipe/RecipeDetails.tsx";
import { RecipeTitleSection } from "../components/section/RecipeTitleSection.tsx";
import { Section } from "../components/section/Section.tsx";
import { Breakpoint } from "../constants/breakpoint.ts";
import { EntityNotFoundError } from "../errors/entity.ts";
import { useExtendedDocumentTitle } from "../hooks/document.ts";
import { useViewedEntity } from "../hooks/sidebar.ts";
import { Machine } from "../model/entity/machine.ts";
import { Recipe } from "../model/entity/recipe.ts";
import { breakpointAtom } from "../state/breakpoint.ts";
import { repositoryAtom } from "../state/combination.ts";
import { optionsAtom } from "../state/options.ts";
import { store } from "../state/store.ts";

const gridColumns = {
    [Breakpoint.Small]: 1,
    [Breakpoint.Medium]: 2,
    [Breakpoint.Large]: 2,
    [Breakpoint.Huge]: 3,
};

type Data = {
    recipe: Recipe;
    machines: Machine[];
};

export const RecipeDetailsPage = () => {
    const { t } = useTranslation(["pages"]);
    const data = useLoaderData() as Data;
    const breakpoint = useAtomValue(breakpointAtom);

    useExtendedDocumentTitle(
        t("pages:recipe-details.title", {
            type: t(`common:entity-type.${data.recipe.type}`),
            label: data.recipe.label,
        }),
    );
    useViewedEntity(data.recipe);

    return (
        <>
            <RecipeTitleSection recipe={data.recipe} />

            <CardSpacer />
            <RecipeDetails recipe={data.recipe} />

            {data.machines.length === 0 ? null : (
                <Section
                    headline={t("pages:recipe-details.machine-list.headline", {
                        count: data.machines.length,
                    })}
                    headIconPath={mdiCogs}
                    spacing
                >
                    <Grid columns={gridColumns[breakpoint]}>
                        {data.machines.map((machine) => (
                            <MachineCard key={machine.key} machine={machine} recipe={data.recipe} />
                        ))}
                    </Grid>
                </Section>
            )}
        </>
    );
};

RecipeDetailsPage.loader = async ({ params }: { params: Params<"type" | "name"> }): Promise<Data> => {
    const repository = store.get(repositoryAtom);
    const options = store.get(optionsAtom);

    const key = `${params.type}.${params.name}`;
    const recipe = await repository.recipes.findByKey(key, options);

    if (!recipe) {
        throw new EntityNotFoundError(params.type!, params.name!);
    }

    const machines = await repository.machines.findForRecipe(recipe, options);

    return {
        recipe: recipe,
        machines: machines,
    };
};

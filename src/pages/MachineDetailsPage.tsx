import { mdiCogs, mdiUpload } from "@mdi/js";
import { useTranslation } from "react-i18next";
import { Params, useLoaderData } from "react-router-dom";
import { CompactRecipeCard } from "../components/card/CompactRecipeCard.tsx";
import { MachineTitleSection } from "../components/section/MachineTitleSection.tsx";
import { Section } from "../components/section/Section.tsx";
import { EntityNotFoundError } from "../errors/entity.ts";
import { useExtendedDocumentTitle } from "../hooks/document.ts";
import { useViewedEntity } from "../hooks/sidebar.ts";
import { Machine } from "../model/entity/machine.ts";
import { RecipeWithTechnology } from "../model/entity/recipe.ts";
import { repositoryAtom } from "../state/combination.ts";
import { optionsAtom } from "../state/options.ts";
import { store } from "../state/store.ts";

type Data = {
    machine: Machine;
    productRecipes: RecipeWithTechnology[];
    supportedRecipes: RecipeWithTechnology[];
};

export const MachineDetailsPage = () => {
    const { t } = useTranslation(["common", "pages"]);
    const data = useLoaderData() as Data;

    useExtendedDocumentTitle(
        t("pages:machine-details.title", {
            label: data.machine.label,
        }),
    );
    useViewedEntity(data.machine);

    return (
        <>
            <MachineTitleSection machine={data.machine} />

            {data.productRecipes.length === 0 ? null : (
                <Section
                    headline={t("pages:machine-details.product-recipe-list.headline", {
                        count: data.productRecipes.length,
                    })}
                    headIconPath={mdiUpload}
                    spacing
                >
                    {data.productRecipes.map((recipe) => (
                        <CompactRecipeCard key={recipe.key} recipe={recipe} spacing />
                    ))}
                </Section>
            )}

            {data.supportedRecipes.length === 0 ? null : (
                <Section
                    headline={t("pages:machine-details.supported-recipe-list.headline", {
                        count: data.supportedRecipes.length,
                    })}
                    headIconPath={mdiCogs}
                    spacing
                >
                    {data.supportedRecipes.map((recipe) => (
                        <CompactRecipeCard key={recipe.key} recipe={recipe} spacing />
                    ))}
                </Section>
            )}
        </>
    );
};

MachineDetailsPage.loader = async ({ params }: { params: Params<"name"> }): Promise<Data> => {
    const repository = store.get(repositoryAtom);
    const options = store.get(optionsAtom);

    const key = `machine.${params.name}`;
    const machine = await repository.machines.findByKey(key, options);

    if (!machine) {
        throw new EntityNotFoundError("machine", params.name!);
    }

    const [productRecipes, supportedRecipes] = await Promise.all([
        await repository.recipes.findWithProduct(`item.${machine.name}`, options),
        await repository.recipes.findForMachine(machine, options),
    ]);

    const [productRecipesWithTechnologies, supportedRecipesWithTechnologies] = await Promise.all([
        repository.technologies.addUnlockingTechnologies(productRecipes, options),
        repository.technologies.addUnlockingTechnologies(supportedRecipes, options),
    ]);

    return {
        machine: machine,
        productRecipes: productRecipesWithTechnologies,
        supportedRecipes: supportedRecipesWithTechnologies,
    };
};

import { mdiDownload, mdiUpload } from "@mdi/js";
import { useTranslation } from "react-i18next";
import { Params, useLoaderData } from "react-router-dom";
import { CompactRecipeCard } from "../components/card/CompactRecipeCard.tsx";
import { ItemTitleSection } from "../components/section/ItemTitleSection.tsx";
import { Section } from "../components/section/Section.tsx";
import { ItemType } from "../constants/entity.ts";
import { EntityNotFoundError } from "../errors/entity.ts";
import { useExtendedDocumentTitle } from "../hooks/document.ts";
import { useViewedEntity } from "../hooks/sidebar.ts";
import { Item } from "../model/entity/item.ts";
import { RecipeWithTechnology } from "../model/entity/recipe.ts";
import { repositoryAtom } from "../state/combination.ts";
import { optionsAtom } from "../state/options.ts";
import { store } from "../state/store.ts";

type Data = {
    item: Item;
    ingredientRecipes: RecipeWithTechnology[];
    productRecipes: RecipeWithTechnology[];
};

export const ItemDetailsPage = () => {
    const { t } = useTranslation(["common", "pages"]);
    const data = useLoaderData() as Data;

    useExtendedDocumentTitle(
        t("pages:item-details.title", {
            type: t(`common:entity-type.${data.item.type}`),
            label: data.item.label,
        }),
    );
    useViewedEntity(data.item);

    return (
        <>
            <ItemTitleSection item={data.item} />

            {data.productRecipes.length === 0 ? null : (
                <Section
                    headline={t("pages:item-details.product-recipe-list.headline", {
                        count: data.productRecipes.length,
                    })}
                    headIconPath={mdiUpload}
                    spacing
                    collapsable
                >
                    {data.productRecipes.map((recipe) => (
                        <CompactRecipeCard key={recipe.key} recipe={recipe} spacing />
                    ))}
                </Section>
            )}

            {data.ingredientRecipes.length === 0 ? null : (
                <Section
                    headline={t("pages:item-details.ingredient-recipe-list.headline", {
                        count: data.ingredientRecipes.length,
                    })}
                    headIconPath={mdiDownload}
                    spacing
                    collapsable
                >
                    {data.ingredientRecipes.map((recipe) => (
                        <CompactRecipeCard key={recipe.key} recipe={recipe} spacing />
                    ))}
                </Section>
            )}
        </>
    );
};

ItemDetailsPage.createLoader = (type: ItemType) => {
    return async ({ params }: { params: Params<"name"> }): Promise<Data> => {
        const repository = store.get(repositoryAtom);
        const options = store.get(optionsAtom);

        const itemKey = `${type}.${params.name}`;
        const [item, ingredientRecipes, productRecipes] = await Promise.all([
            repository.items.findByKey(itemKey, options),
            repository.recipes.findWithIngredient(itemKey, options),
            repository.recipes.findWithProduct(itemKey, options),
        ]);

        if (!item) {
            throw new EntityNotFoundError(type, params.name!);
        }

        const [ingredientRecipesWithTechnologies, productRecipesWithTechnologies] = await Promise.all([
            repository.technologies.addUnlockingTechnologies(ingredientRecipes, options),
            repository.technologies.addUnlockingTechnologies(productRecipes, options),
        ]);

        return {
            item: item,
            ingredientRecipes: ingredientRecipesWithTechnologies,
            productRecipes: productRecipesWithTechnologies,
        };
    };
};

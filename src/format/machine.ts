export function formatCraftingSpeed(speed: number): string {
    return Math.round(speed * 100) / 100 + "x";
}

export function formatMachineSlots(slots: number): string {
    if (slots === 0) {
        return "--";
    }
    if (slots === 255) {
        return "∞";
    }
    return `${slots}`;
}

export function formatEnergyUsage(energyUsage: number, energyUsageUnit: string): string {
    if (!energyUsage) {
        return "--";
    }

    return energyUsage + energyUsageUnit;
}

import { atom } from "jotai";
import { atomWithStorage } from "jotai/utils";
import { Repository } from "../model/repository/repository.ts";

// The atom holding the combination id currently in use.
export const combinationIdAtom = atomWithStorage<string>(
    "last-combination-id",
    "vanilla",
    {
        getItem(storageKey, initialValue) {
            return localStorage.getItem(storageKey) || initialValue;
        },
        setItem(storageKey, value) {
            localStorage.setItem(storageKey, value);
        },
        removeItem(storageKey) {
            localStorage.removeItem(storageKey);
        },
    },
    {
        getOnInit: true,
    },
);

// The read-only atom providing the repository of the currently active combination.
export const repositoryAtom = atom((get) => {
    const combinationId = get(combinationIdAtom);
    return new Repository(combinationId);
});

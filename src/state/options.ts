import { atom } from "jotai";
import { atomFamily, atomWithStorage } from "jotai/utils";
import { Options } from "../model/entity/options.ts";
import { combinationIdAtom } from "./combination.ts";

const defaultOptions: Options = {
    locale: "en",
    showHidden: false,
};

const optionsFamily = atomFamily((combinationId: string) => {
    return atomWithStorage(`${combinationId}.options`, defaultOptions, undefined, {
        getOnInit: true,
    });
});

export const optionsAtom = atom((get) => {
    const combinationId = get(combinationIdAtom);
    return get(optionsFamily(combinationId));
});

import { atom } from "jotai";
import { atomFamily, atomWithStorage, unwrap } from "jotai/utils";
import { Entity, SidebarEntity } from "../model/entity/entity.ts";
import { Options } from "../model/entity/options.ts";
import { Repository } from "../model/repository/repository.ts";
import { combinationIdAtom, repositoryAtom } from "./combination.ts";
import { optionsAtom } from "./options.ts";

type SidebarEntities = Record<string, SidebarEntity>;
type StoredEntities = Record<string, [number, number]>;

// Fetches the full entities for the provided stored ones.
async function fetchEntities(
    repository: Repository,
    options: Options,
    storedEntities: StoredEntities,
): Promise<SidebarEntities> {
    const [items, machines, recipes] = await Promise.all([
        repository.items.findByKeys(Object.keys(storedEntities), options),
        repository.machines.findByKeys(Object.keys(storedEntities), options),
        repository.recipes.findByKeys(Object.keys(storedEntities), options),
    ]);
    const entities = {
        ...items,
        ...machines,
        ...recipes,
    };

    const sidebarEntities: SidebarEntities = {};
    for (const [key, [timestamp, position]] of Object.entries(storedEntities)) {
        if (!entities[key]) {
            continue;
        }

        sidebarEntities[key] = {
            ...entities[key],
            timestamp: timestamp,
            position: position,
        };
    }

    return sidebarEntities;
}

// Normalizes the provided entities, i.e. limits the number of viewed entities, checks the position to be without gaps.
function normalizeEntities(entities: SidebarEntities): SidebarEntities {
    // Limit viewed (not marked as favorite) entities to maximum of 10 entries.
    Object.values(entities)
        .filter((entity) => entity.position === 0)
        .sort((a, b) => b.timestamp - a.timestamp)
        .slice(10)
        .forEach((entity) => {
            delete entities[entity.key];
        });

    // Re-order the favorites, placing entities with -1 at the very end.
    Object.values(entities)
        .filter((entity) => entity.position !== 0)
        .sort((a, b) => (a.position === -1 ? 1 : b.position === -1 ? -1 : a.position - b.position))
        .forEach((entity, idx) => {
            entity.position = idx + 1;
        });

    return entities;
}

// The atom family for storing the sidebar entities in the local storage, separate for each combination.
const storedEntitiesFamily = atomFamily((combinationId: string) => {
    const storageKey = `${combinationId}.sidebar-entities`;
    return atomWithStorage(storageKey, {} as StoredEntities, undefined, {
        getOnInit: true,
    });
});

// The main atom for the sidebar entities. This atom will manage a single list for all entities in the sidebar.
// Derived atoms will separate them into the respective lists.
const sidebarEntitiesAtom = atom<Promise<SidebarEntities>, [SidebarEntities], void>(
    async (get): Promise<SidebarEntities> => {
        const combinationId = get(combinationIdAtom);
        const repository = get(repositoryAtom);
        const options = get(optionsAtom);
        const storedEntities = get(storedEntitiesFamily(combinationId));

        return await fetchEntities(repository, options, storedEntities);
    },
    (get, set, sidebarEntities: SidebarEntities) => {
        const combinationId = get(combinationIdAtom);

        const storedEntities: StoredEntities = {};
        for (const [key, sidebarEntity] of Object.entries(sidebarEntities)) {
            storedEntities[key] = [sidebarEntity.timestamp, sidebarEntity.position];
        }

        return set(storedEntitiesFamily(combinationId), storedEntities);
    },
);

const unwrappedSidebarEntitiesAtom = unwrap(sidebarEntitiesAtom, (prev) => prev ?? {});

// The read-only atom providing the list of entities marked as favorite. The returned array is already sorted by the
// user-defined position of the entities.
export const sidebarFavoriteEntitiesAtom = atom((get) => {
    return Object.values(get(unwrappedSidebarEntitiesAtom))
        .filter((entity) => entity.position > 0)
        .sort((a, b) => a.position - b.position);
});

// The read-only atom providing the list of last-viewed entities, which are not marked as favorite.
// The returned array is already sorted by view-time.
export const sidebarLastViewedEntitiesAtom = atom((get) => {
    return Object.values(get(unwrappedSidebarEntitiesAtom))
        .filter((entity) => entity.position === 0)
        .sort((a, b) => b.timestamp - a.timestamp);
});

// The write-only atom to add an entity to the list of viewed ones. If the entity is already present, it will be
// updated. If the entity is marked as a favorite, it will remain a favorite.
export const addViewedEntityAtom = atom(null, async (get, set, entity: Entity) => {
    const entities = {
        ...(await get(sidebarEntitiesAtom)),
    };
    entities[entity.key] = {
        ...entity,
        timestamp: Date.now(),
        position: entities[entity.key]?.position || 0,
    };

    set(sidebarEntitiesAtom, normalizeEntities(entities));
});

// The write-only atom to add an entity to the favorites.
export const addEntityToFavoritesAtom = atom(null, async (get, set, entity: Entity) => {
    const entities = {
        ...(await get(sidebarEntitiesAtom)),
    };

    if (!entities[entity.key]) {
        entities[entity.key] = {
            ...entity,
            timestamp: Date.now(),
            position: -1,
        };
    } else if (entities[entity.key].position === 0) {
        entities[entity.key].position = -1; // Will be sorted in by the normalizer.
    }

    set(sidebarEntitiesAtom, normalizeEntities(entities));
});

// The write-only atom to remove an entity from the favorites.
export const removeEntityFromFavoritesAtom = atom(null, async (get, set, entity: Entity) => {
    const entities = {
        ...(await get(sidebarEntitiesAtom)),
    };

    if (entities[entity.key]) {
        entities[entity.key].position = 0;
    }

    set(sidebarEntitiesAtom, normalizeEntities(entities));
});

// The write-only atom to set the order of ALL entities marked as favorite.
export const setFavoritesOrderAtom = atom(null, async (get, set, orderedEntities: Entity[]) => {
    const orderedKeys = orderedEntities.map((entity) => entity.key);
    const entities = {
        ...(await get(sidebarEntitiesAtom)),
    };

    for (const key of Object.keys(entities)) {
        entities[key].position = orderedKeys.indexOf(key) + 1;
    }

    set(sidebarEntitiesAtom, entities);
});

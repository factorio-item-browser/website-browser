import { atom } from "jotai";
import { Breakpoint } from "../constants/breakpoint.ts";

const mediaQueries = {
    [Breakpoint.Small]: "(max-width: 799px)",
    [Breakpoint.Medium]: "(min-width: 800px) and (max-width: 1199px)",
    [Breakpoint.Large]: "(min-width: 1200px) and (max-width: 1499px)",
    [Breakpoint.Huge]: "(min-width: 1500px)",
};

export const breakpointAtom = atom<Breakpoint>(Breakpoint.Huge);

breakpointAtom.onMount = (setAtom) => {
    const handlers: Record<string, (event: MediaQueryListEvent) => void> = {};

    for (const breakpoint of Object.values(Breakpoint)) {
        const handler = (event: MediaQueryListEvent) => {
            if (event.matches) {
                setAtom(breakpoint);
            }
        };
        handlers[mediaQueries[breakpoint]] = handler;

        const match = window.matchMedia(mediaQueries[breakpoint]);
        match.addEventListener("change", handler);

        if (match.matches) {
            setAtom(breakpoint);
        }
    }

    return () => {
        for (const [mediaQuery, handler] of Object.entries(handlers)) {
            window.matchMedia(mediaQuery).removeEventListener("change", handler);
        }
    };
};

import { atom } from "jotai";
import { Theme } from "../constants/theme.ts";

export const themeAtom = atom<Theme>(Theme.System);

themeAtom.onMount = (setAtom) => {
    const handler = (event: MediaQueryListEvent) => {
        setAtom(event.matches ? Theme.Dark : Theme.Light);
    };
    const match = window.matchMedia("(prefers-color-scheme: dark)");
    match.addEventListener("change", handler);

    // Initialize with correct value.
    setAtom(match.matches ? Theme.Dark : Theme.Light);

    return () => {
        match.removeEventListener("change", handler);
    };
};

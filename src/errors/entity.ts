import { TFunction } from "i18next";
import { AbstractError } from "./abstract.ts";

/**
 * The error thrown when an unknown entity has been encountered.
 */
export class EntityNotFoundError extends AbstractError {
    public constructor(
        public readonly type: string,
        public readonly name: string,
    ) {
        super();
    }

    public getTitle(t: TFunction): string {
        const entity = t(`common:entity-type.${this.type}`);

        return t(`errors:entity-not-found.title`, { entity: entity });
    }

    public getMessage(t: TFunction): string {
        return t(`errors:entity-not-found.message`, { name: this.name });
    }
}

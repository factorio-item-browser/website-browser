import { TFunction } from "i18next";

export abstract class AbstractError {
    /**
     * Returns the translated title to use for the error.
     */
    public abstract getTitle(t: TFunction): string;

    /**
     * Returns the translated message to use for the error.
     */
    public abstract getMessage(t: TFunction): string;
}

import { TechnologyType } from "../../constants/entity.ts";
import { Entity } from "./entity.ts";
import { ItemWithAmount } from "./item.ts";

export type Technology = Entity<TechnologyType> & {
    prerequisites: string[];
    researchIngredients: ItemWithAmount[];
    researchCount: number;
    researchCountFormula: string;
    unlockedRecipes: string[];
    level: number;
    maxLevel: number;
};

import { EntityType } from "../../constants/entity.ts";

export type Entity<T = EntityType> = {
    key: string;
    type: T;
    name: string;
    label: string;
    description: string;
    iconHash: string;
};

export type SidebarEntity = Entity & {
    timestamp: number;
    position: number;
};

// The options used by the repositories to select data.
export type Options = {
    locale: string;
    showHidden: boolean;
};

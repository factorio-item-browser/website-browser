import { MachineType } from "../../constants/entity.ts";
import { Entity } from "./entity.ts";

export type Machine = Entity<MachineType> & {
    craftingCategories: string[];
    resourceCategories: string[];
    speed: number;
    itemSlots: number;
    fluidInputSlots: number;
    fluidOutputSlots: number;
    moduleSlots: number;
    energyUsage: number;
    energyUsageUnit: string;
};

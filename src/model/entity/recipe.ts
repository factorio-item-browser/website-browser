import { RecipeType } from "../../constants/entity.ts";
import { Entity } from "./entity.ts";
import { ItemWithAmount } from "./item.ts";
import { Technology } from "./technology.ts";

export type Recipe = Entity<RecipeType> & {
    category: string;
    time: number;
    ingredients: ItemWithAmount[];
    products: ItemWithAmount[];
    hidden: boolean;
};

export type RecipeWithTechnology = Recipe & {
    technology: Technology | null;
};

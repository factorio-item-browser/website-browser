import { ModType } from "../../constants/entity.ts";
import { Entity } from "./entity.ts";

export type Mod = Entity<ModType> & {
    author: string;
    version: string;
};

import { ItemType } from "../../constants/entity.ts";
import { Entity } from "./entity.ts";

export type Item = Entity<ItemType> & {
    stackSize: number;
    hidden: boolean;
};

export type ItemWithAmount = Item & {
    amount: number;
};

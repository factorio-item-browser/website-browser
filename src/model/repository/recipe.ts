import Dexie from "dexie";
import { RecipeType } from "../../constants/entity.ts";
import { Item, ItemWithAmount } from "../entity/item.ts";
import { Machine } from "../entity/machine.ts";
import { Options } from "../entity/options.ts";
import { Recipe } from "../entity/recipe.ts";
import { AbstractRepository } from "./abstract.ts";
import { createItemsWithAmount, isRecipeCompatibleToMachine, parseKey, translate, Translations } from "./helper.ts";
import { ItemRepository } from "./item.ts";

export type DatabaseRecipe = {
    key: string;
    labels: Translations;
    descriptions: Translations;
    iconHash: string;
    category: string;
    time: number;
    ingredientKeys: string[];
    ingredientAmounts: number[];
    productKeys: string[];
    productAmounts: number[];
    enabled: boolean;
    hidden: boolean;
};

export class RecipeRepository extends AbstractRepository<Recipe, DatabaseRecipe> {
    public constructor(
        table: Dexie.Table<DatabaseRecipe, string>,
        private readonly itemRepository: ItemRepository,
    ) {
        super(table);
    }

    protected async map(
        recipe: DatabaseRecipe,
        options: Options,
        prefetchedItems?: Record<string, Item>,
    ): Promise<Recipe> {
        if (!prefetchedItems) {
            prefetchedItems = await this.fetchRecipeItems([recipe], options);
        }

        const [type, name] = parseKey(recipe.key);
        return {
            key: recipe.key,
            type: type as RecipeType,
            name: name,
            label: translate(recipe.labels, options.locale, recipe.key),
            description: translate(recipe.descriptions, options.locale),
            iconHash: recipe.iconHash,
            category: recipe.category,
            time: recipe.time,
            ingredients: createItemsWithAmount(recipe.ingredientKeys, recipe.ingredientAmounts, prefetchedItems),
            products: createItemsWithAmount(recipe.productKeys, recipe.productAmounts, prefetchedItems),
            hidden: recipe.hidden,
        };
    }

    protected async mapArray(recipes: DatabaseRecipe[], options: Options): Promise<Recipe[]> {
        const prefetchedItems = await this.fetchRecipeItems(recipes, options);
        const result: Recipe[] = [];
        for (const recipe of recipes) {
            result.push(await this.map(recipe, options, prefetchedItems));
        }
        return result;
    }

    /**
     * Fetches all items (ingredients and products) of the provided recipes from the database.
     */
    private async fetchRecipeItems(recipes: DatabaseRecipe[], options: Options): Promise<Record<string, Item>> {
        const itemKeys: string[] = [];
        for (const recipe of recipes) {
            itemKeys.push(...recipe.ingredientKeys, ...recipe.productKeys);
        }

        return await this.itemRepository.findByKeys(itemKeys, options);
    }

    /**
     * Finds all recipes with the provided ingredient. The ingredients and products of the recipes will be sorted to
     * prefer the specified key.
     */
    public async findWithIngredient(itemKey: string, options: Options): Promise<Recipe[]> {
        const databaseRecipes = await this.table
            .where("ingredientKeys")
            .equals(itemKey)
            .filter((recipe) => !recipe.hidden || options.showHidden)
            .toArray();

        const recipes = await this.mapArray(databaseRecipes, options);
        for (const recipe of recipes) {
            this.sortIngredientsAndProducts(recipe, itemKey);
        }
        return this.sortRecipes(recipes, (recipe) => recipe.ingredients.length);
    }

    /**
     * Finds all recipes with the provided product. The ingredients and products of the recipes will be sorted to
     * prefer the specified key.
     */
    public async findWithProduct(itemKey: string, options: Options): Promise<Recipe[]> {
        const databaseRecipes = await this.table
            .where("productKeys")
            .equals(itemKey)
            .filter((recipe) => !recipe.hidden || options.showHidden)
            .toArray();

        const recipes = await this.mapArray(databaseRecipes, options);
        for (const recipe of recipes) {
            this.sortIngredientsAndProducts(recipe, itemKey);
        }
        return this.sortRecipes(recipes, (recipe) => recipe.products.length);
    }

    /**
     * Sorts the ingredients and products of the provided recipe, preferring the provided key to be sorted to the front.
     */
    private sortIngredientsAndProducts(recipe: Recipe, preferredItemKey: string): void {
        const compare = (left: ItemWithAmount, right: ItemWithAmount): number => {
            if (left.key === preferredItemKey) {
                return -1;
            }
            if (right.key === preferredItemKey) {
                return 1;
            }
            return 0;
        };

        recipe.ingredients.sort(compare);
        recipe.products.sort(compare);
    }

    /**
     * Sorts the recipes after the number of items, calculated by the provided callback.
     */
    private sortRecipes(recipes: Recipe[], itemCounter: (recipe: Recipe) => number): Recipe[] {
        const compare = (left: Recipe, right: Recipe): number => {
            const leftCount = itemCounter(left);
            const rightCount = itemCounter(right);
            if (leftCount === rightCount) {
                return left.key.localeCompare(right.key);
            }
            return leftCount - rightCount;
        };

        recipes.sort(compare);
        return recipes;
    }

    /**
     * Finds and returns all recipes which can be produced by the provided machine.
     */
    public async findForMachine(machine: Machine, options: Options): Promise<Recipe[]> {
        const [craftingRecipes, miningRecipes] = await Promise.all([
            this.table
                .where("category")
                .anyOf(machine.craftingCategories)
                .filter((recipe) => recipe.key.startsWith(RecipeType.Crafting))
                .filter((recipe) => !recipe.hidden || options.showHidden)
                .toArray(),
            this.table
                .where("category")
                .anyOf(machine.resourceCategories)
                .filter((recipe) => recipe.key.startsWith(RecipeType.Mining))
                .filter((recipe) => !recipe.hidden || options.showHidden)
                .toArray(),
        ]);

        const recipes = await this.mapArray([...craftingRecipes, ...miningRecipes], options);
        const filteredRecipes = recipes.filter((recipe) => isRecipeCompatibleToMachine(recipe, machine));
        return this.sortRecipes(filteredRecipes, (recipe) => 1000 * recipe.ingredients.length + recipe.products.length);
    }
}

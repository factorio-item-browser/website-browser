import { ItemType } from "../../constants/entity.ts";
import { Item } from "../entity/item.ts";
import { Options } from "../entity/options.ts";
import { AbstractRepository } from "./abstract.ts";
import { parseKey, selectRandomNumbers, shuffle, translate, Translations } from "./helper.ts";

export type DatabaseItem = {
    key: string;
    labels: Translations;
    descriptions: Translations;
    iconHash: string;
    stackSize: number;
    hidden: boolean;
};

export class ItemRepository extends AbstractRepository<Item, DatabaseItem> {
    protected async map(item: DatabaseItem, options: Options): Promise<Item> {
        const [type, name] = parseKey(item.key);
        return {
            key: item.key,
            type: type as ItemType,
            name: name,
            label: translate(item.labels, options.locale, item.key),
            description: translate(item.descriptions, options.locale),
            iconHash: item.iconHash,
            stackSize: item.stackSize,
            hidden: item.hidden,
        };
    }

    /**
     * Finds and returns all items, sorted by their internal name.
     */
    public async findAll(options: Options): Promise<Item[]> {
        const items = await this.table.filter((item) => !item.hidden || options.showHidden).toArray();
        const mappedItems = await this.mapArray(items, options);
        mappedItems.sort((left: Item, right: Item) => left.name.localeCompare(right.name));
        return mappedItems;
    }

    /**
     * Finds and returns the provided number of random items.
     */
    public async findRandom(numberOfResults: number, options: Options): Promise<Item[]> {
        const numberOfItems = await this.table.count();
        const indexes = selectRandomNumbers(numberOfItems, numberOfResults).sort((a, b) => a - b);

        const items: DatabaseItem[] = [];
        let currentIndex = 0;
        await this.table
            .toCollection()
            .until((item) => {
                if (indexes.length == 0) {
                    return true;
                }

                if (currentIndex === indexes[0]) {
                    items.push(item);
                    indexes.shift();
                }

                ++currentIndex;
                return false;
            })
            .count();

        return this.mapArray(shuffle(items), options);
    }
}

import { Table } from "dexie";
import { Options } from "../entity/options.ts";
import { arrayToObject, filterNotEmpty } from "./helper.ts";

type MinimalEntity = {
    key: string;
};

/**
 * The abstract class of the repositories with some common logic.
 */
export abstract class AbstractRepository<TEntity extends MinimalEntity, TDatabaseEntity> {
    public constructor(protected readonly table: Table<TDatabaseEntity, string>) {}

    /**
     * Maps the database entity to a data entity.
     */
    protected abstract map(entity: TDatabaseEntity, options: Options): Promise<TEntity>;

    /**
     * Maps an array of database entities to their data entities.
     */
    protected async mapArray(entities: TDatabaseEntity[], options: Options): Promise<TEntity[]> {
        const result: TEntity[] = [];
        for (const entity of entities) {
            result.push(await this.map(entity, options));
        }
        return result;
    }

    /**
     * Finds the entity of the provided key. Returns null if the key is not known.
     */
    public async findByKey(key: string, options: Options): Promise<TEntity | null> {
        const entity = await this.table.get(key);
        return entity ? this.map(entity, options) : null;
    }

    /**
     * Finds multiple entities by their keys. The returned record will have the entities assigned by their keys. If
     * a key is not known, it will not appear in the returned record.
     */
    public async findByKeys(keys: string[], options: Options): Promise<Record<string, TEntity>> {
        const entities = await this.table.where("key").startsWithAnyOf(keys).toArray();
        const mappedEntities = await this.mapArray(filterNotEmpty(entities), options);
        return arrayToObject(mappedEntities);
    }
}

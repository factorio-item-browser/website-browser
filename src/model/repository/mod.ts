import { ModType } from "../../constants/entity.ts";
import { Mod } from "../entity/mod.ts";
import { Options } from "../entity/options.ts";
import { AbstractRepository } from "./abstract.ts";
import { parseKey, translate, Translations } from "./helper.ts";

export type DatabaseMod = {
    key: string;
    labels: Translations;
    descriptions: Translations;
    author: string;
    version: string;
    iconHash: string;
};

export class ModRepository extends AbstractRepository<Mod, DatabaseMod> {
    protected async map(mod: DatabaseMod, options: Options): Promise<Mod> {
        const [type, name] = parseKey(mod.key);
        return {
            key: mod.key,
            type: type as ModType,
            name: name,
            label: translate(mod.labels, options.locale, mod.key),
            description: translate(mod.descriptions, options.locale),
            author: mod.author,
            version: mod.version,
            iconHash: mod.iconHash,
        };
    }

    /**
     * Finds and returns all mods, sorted by their internal name.
     */
    public async findAll(options: Options): Promise<Mod[]> {
        const mods = await this.table.toArray();
        const mappedMods = await this.mapArray(mods, options);
        mappedMods.sort((left: Mod, right: Mod) => left.name.localeCompare(right.name));
        return mappedMods;
    }
}

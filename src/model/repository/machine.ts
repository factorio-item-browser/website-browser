import { MachineType, RecipeType } from "../../constants/entity.ts";
import { Machine } from "../entity/machine.ts";
import { Options } from "../entity/options.ts";
import { Recipe } from "../entity/recipe.ts";
import { AbstractRepository } from "./abstract.ts";
import { isRecipeCompatibleToMachine, parseKey, translate, Translations } from "./helper.ts";

export type DatabaseMachine = {
    key: string;
    labels: Translations;
    descriptions: Translations;
    iconHash: string;
    craftingCategories: string[];
    resourceCategories: string[];
    speed: number;
    itemSlots: number;
    fluidInputSlots: number;
    fluidOutputSlots: number;
    moduleSlots: number;
    energyUsage: number;
    energyUsageUnit: string;
};

export class MachineRepository extends AbstractRepository<Machine, DatabaseMachine> {
    protected async map(machine: DatabaseMachine, options: Options): Promise<Machine> {
        const [type, name] = parseKey(machine.key);
        return {
            key: machine.key,
            type: type as MachineType,
            name: name,
            label: translate(machine.labels, options.locale, machine.key),
            description: translate(machine.descriptions, options.locale),
            iconHash: machine.iconHash,
            craftingCategories: machine.craftingCategories,
            resourceCategories: machine.resourceCategories,
            speed: machine.speed,
            itemSlots: machine.itemSlots,
            fluidInputSlots: machine.fluidInputSlots,
            fluidOutputSlots: machine.fluidOutputSlots,
            moduleSlots: machine.moduleSlots,
            energyUsage: machine.energyUsage,
            energyUsageUnit: machine.energyUsageUnit,
        };
    }

    /**
     * Finds and returns all machines which are able to produce the provided recipe.
     */
    public async findForRecipe(recipe: Recipe, options: Options): Promise<Machine[]> {
        let machines: DatabaseMachine[] = [];
        if (recipe.type === RecipeType.Crafting) {
            machines = await this.table.where("craftingCategories").equals(recipe.category).toArray();
        } else if (recipe.type === RecipeType.Mining) {
            machines = await this.table.where("resourceCategories").equals(recipe.category).toArray();
        }

        const mappedMachines = await this.mapArray(machines, options);
        const filteredMachines = mappedMachines.filter((machine) => isRecipeCompatibleToMachine(recipe, machine));
        return this.sortMachines(filteredMachines);
    }

    /**
     * Sorts the provided machines, preferring the character and placing him to the front.
     */
    private sortMachines(machines: Machine[]): Machine[] {
        const compare = (left: Machine, right: Machine): number => {
            if (left.name === "character") {
                return -1;
            }

            if (right.name === "character") {
                return 1;
            }

            return left.name.localeCompare(right.name);
        };

        machines.sort(compare);
        return machines;
    }
}

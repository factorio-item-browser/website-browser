import Dexie, { Table } from "dexie";
import { ModType, TechnologyType } from "../../constants/entity.ts";
import { DataReader, ExportData } from "../export/reader.ts";
import { DatabaseItem, ItemRepository } from "./item.ts";
import { DatabaseMachine, MachineRepository } from "./machine.ts";
import { DatabaseMod, ModRepository } from "./mod.ts";
import { DatabaseRecipe, RecipeRepository } from "./recipe.ts";
import { DatabaseTechnology, TechnologyRepository } from "./technology.ts";

class Database extends Dexie {
    public items!: Table<DatabaseItem, string>;
    public machines!: Table<DatabaseMachine, string>;
    public mods!: Table<DatabaseMod, string>;
    public recipes!: Table<DatabaseRecipe, string>;
    public technologies!: Table<DatabaseTechnology, string>;

    public constructor(combinationId: string) {
        super(combinationId);

        this.version(1).stores({
            items: "key",
            machines: "key,*craftingCategories,*resourceCategories",
            mods: "key",
            recipes: "key,*ingredientKeys,*productKeys,category",
            technologies: "key,*prerequisites,*researchIngredientKeys,*unlockedRecipes",
        });
    }
}

export class Repository {
    private database: Database;

    public readonly items: ItemRepository;
    public readonly machines: MachineRepository;
    public readonly mods: ModRepository;
    public readonly recipes: RecipeRepository;
    public readonly technologies: TechnologyRepository;

    public constructor(combinationId: string) {
        this.database = new Database(combinationId);

        this.items = new ItemRepository(this.database.items);
        this.machines = new MachineRepository(this.database.machines);
        this.mods = new ModRepository(this.database.mods);
        this.recipes = new RecipeRepository(this.database.recipes, this.items);
        this.technologies = new TechnologyRepository(this.database.technologies, this.items);
    }

    public async initializeDatabase(): Promise<void> {
        console.log("Close database");
        this.database.close();
        console.log("Delete database");
        await this.database.delete();
        console.log("Open database");
        await this.database.open();

        console.log("Fetch data");
        console.log(import.meta.env.VITE_FIB_BASE_URL);
        const response = await fetch(`${import.meta.env.VITE_FIB_BASE_URL}/data/${this.database.name}.jsonl`);
        if (!response.body) {
            return;
        }

        const reader = new DataReader(response.body);
        reader.onData = (data: ExportData) => {
            switch (data.kind) {
                case "item.v1": {
                    this.database.items.add({
                        key: `${data.type}.${data.name}`,
                        labels: data.labels,
                        descriptions: data.descriptions,
                        iconHash: data.icon,
                        stackSize: data.stackSize,
                        hidden: data.hidden,
                    });
                    break;
                }

                case "machine.v1": {
                    this.database.machines.add({
                        key: `machine.${data.name}`,
                        labels: data.labels,
                        descriptions: data.descriptions,
                        iconHash: data.icon,
                        craftingCategories: data.craftingCategories,
                        resourceCategories: data.resourceCategories,
                        speed: data.speed,
                        itemSlots: data.itemSlots,
                        fluidInputSlots: data.fluidInputSlots,
                        fluidOutputSlots: data.fluidOutputSlots,
                        moduleSlots: data.moduleSlots,
                        energyUsage: data.energyUsage,
                        energyUsageUnit: data.energyUsageUnit,
                    });
                    break;
                }

                case "mod.v1": {
                    this.database.mods.add({
                        key: `${ModType.Mod}.${data.name}`,
                        labels: data.labels,
                        descriptions: data.descriptions,
                        author: data.author,
                        version: data.version,
                        iconHash: data.icon,
                    });
                    break;
                }

                case "recipe.v1": {
                    if (data.name === "electric-energy-interface") {
                        console.log("data", data);
                    }

                    const recipe: DatabaseRecipe = {
                        key: `${data.type}.${data.name}`,
                        labels: data.labels,
                        descriptions: data.descriptions,
                        iconHash: data.icon,
                        category: data.category,
                        time: data.time,
                        ingredientKeys: [],
                        ingredientAmounts: [],
                        productKeys: [],
                        productAmounts: [],
                        enabled: data.enabled,
                        hidden: data.hidden,
                    };

                    for (const ingredient of data.ingredients) {
                        recipe.ingredientKeys.push(`${ingredient.type}.${ingredient.name}`);
                        recipe.ingredientAmounts.push(ingredient.amount);
                    }

                    for (const product of data.products) {
                        recipe.productKeys.push(`${product.type}.${product.name}`);
                        recipe.productAmounts.push(((product.amountMax + product.amountMin) / 2) * product.probability);
                    }

                    this.database.recipes.add(recipe);
                    break;
                }

                case "technology.v1": {
                    const technology: DatabaseTechnology = {
                        key: `${TechnologyType.Technology}.${data.name}`,
                        labels: data.labels,
                        descriptions: data.descriptions,
                        iconHash: data.icon,
                        prerequisites: data.prerequisites,
                        researchIngredientKeys: [],
                        researchIngredientAmounts: [],
                        researchCount: data.researchCount,
                        researchCountFormula: data.researchCountFormula,
                        unlockedRecipes: data.unlockedRecipes,
                        level: data.level,
                        maxLevel: data.maxLevel,
                    };

                    for (const ingredient of data.researchIngredients) {
                        technology.researchIngredientKeys.push(`${ingredient.type}.${ingredient.name}`);
                        technology.researchIngredientAmounts.push(ingredient.amount);
                    }

                    this.database.technologies.add(technology);
                    break;
                }
            }
        };
        await reader.read();

        console.log("Done");
    }
}

import { ItemType } from "../../constants/entity.ts";
import { Item, ItemWithAmount } from "../entity/item.ts";
import { Machine } from "../entity/machine.ts";
import { Recipe } from "../entity/recipe.ts";

export type Translations = Record<string, string>;

export const defaultLocale = "en";

/**
 * Parses the provided key to retrieve the type and name from it.
 */
export function parseKey(key: string, withMode?: boolean): [string, string, string] {
    const parts = key.split(".");
    const type = parts.shift() || "";
    const mode = withMode ? parts.pop() || "" : "";
    const name = parts.join(".");

    return [type, name, mode];
}

/**
 * Extracts the correct translation from the provided ones, preferring the provided locale.
 */
export function translate(translations: Translations, locale: string, fallback: string = ""): string {
    return translations[locale] || translations[defaultLocale] || fallback;
}

/**
 * Transforms the provided array of entities to an object, using the entity keys as keys for the object.
 */
export function arrayToObject<T extends { key: string }>(entities: T[]): Record<string, T> {
    const result: Record<string, T> = {};
    for (const entity of entities) {
        result[entity.key] = entity;
    }
    return result;
}

/**
 * Filters any empty values from the provided array, returning only the actually not-empty values.
 */
export function filterNotEmpty<T>(entities: (T | null | undefined)[]): T[] {
    return entities.filter((entity): entity is T => {
        return entity !== undefined && entity !== null;
    });
}

/**
 * Selects random numbers between 0 and max.
 */
export function selectRandomNumbers(max: number, count: number): number[] {
    if (count > max) {
        count = max;
    }

    const result = [];
    while (result.length < count) {
        const idx = Math.floor(Math.random() * max);
        if (result.indexOf(idx) === -1) {
            result.push(idx);
        }
    }
    return result;
}

/**
 * Shuffles the provided items and returns them.
 */
export function shuffle<T>(items: T[]): T[] {
    for (let i = items.length - 1; i > 0; i--) {
        const j = Math.floor(Math.random() * (i + 1));
        [items[i], items[j]] = [items[j], items[i]];
    }
    return items;
}

/**
 * Checks whether the provided recipe can actually be crafted in the provided machine.
 */
export function isRecipeCompatibleToMachine(recipe: Recipe, machine: Machine): boolean {
    // todo Add category check?

    const countItems = recipe.ingredients.filter((ingredient) => ingredient.type === ItemType.Item).length;
    const countInputFluids = recipe.ingredients.filter((ingredient) => ingredient.type === ItemType.Fluid).length;
    const countOutputFluids = recipe.products.filter((product) => product.type === ItemType.Fluid).length;

    return (
        machine.itemSlots >= countItems &&
        machine.fluidInputSlots >= countInputFluids &&
        machine.fluidOutputSlots >= countOutputFluids
    );
}

/**
 * Creates the items with their amounts, combining the provided keys and amounts together. All items must already be
 * prefetched from the database.
 */
export function createItemsWithAmount(
    keys: string[],
    amounts: number[],
    items: Record<string, Item>,
): ItemWithAmount[] {
    const result: ItemWithAmount[] = [];
    for (let i = 0; i < keys.length; ++i) {
        const item = items[keys[i]];
        if (!item || !amounts[i]) {
            continue;
        }

        result.push({
            ...item,
            amount: amounts[i],
        });
    }
    return result;
}

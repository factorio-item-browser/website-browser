import Dexie from "dexie";
import { TechnologyType } from "../../constants/entity.ts";
import { Item } from "../entity/item.ts";
import { Options } from "../entity/options.ts";
import { Recipe, RecipeWithTechnology } from "../entity/recipe.ts";
import { Technology } from "../entity/technology.ts";
import { AbstractRepository } from "./abstract.ts";
import { createItemsWithAmount, parseKey, translate, Translations } from "./helper.ts";
import { ItemRepository } from "./item.ts";

export type DatabaseTechnology = {
    key: string;
    labels: Translations;
    descriptions: Translations;
    iconHash: string;
    prerequisites: string[];
    researchIngredientKeys: string[];
    researchIngredientAmounts: number[];
    researchCount: number;
    researchCountFormula: string;
    unlockedRecipes: string[];
    level: number;
    maxLevel: number;
};

export class TechnologyRepository extends AbstractRepository<Technology, DatabaseTechnology> {
    public constructor(
        table: Dexie.Table<DatabaseTechnology, string>,
        private readonly itemRepository: ItemRepository,
    ) {
        super(table);
    }

    protected async map(
        technology: DatabaseTechnology,
        options: Options,
        prefetchedItems?: Record<string, Item>,
    ): Promise<Technology> {
        if (!prefetchedItems) {
            prefetchedItems = await this.fetchTechnologyItems([technology], options);
        }

        const [type, name] = parseKey(technology.key);
        return {
            key: technology.key,
            type: type as TechnologyType,
            name: name,
            label: translate(technology.labels, options.locale, technology.key),
            description: translate(technology.descriptions, options.locale),
            iconHash: technology.iconHash,
            prerequisites: technology.prerequisites,
            researchIngredients: createItemsWithAmount(
                technology.researchIngredientKeys,
                technology.researchIngredientAmounts,
                prefetchedItems,
            ),
            researchCount: technology.researchCount,
            researchCountFormula: technology.researchCountFormula,
            unlockedRecipes: technology.unlockedRecipes,
            level: technology.level,
            maxLevel: technology.maxLevel,
        };
    }

    protected async mapArray(technologies: DatabaseTechnology[], options: Options): Promise<Technology[]> {
        const prefetchedItems = await this.fetchTechnologyItems(technologies, options);
        const result: Technology[] = [];
        for (const technology of technologies) {
            result.push(await this.map(technology, options, prefetchedItems));
        }
        return result;
    }

    /**
     * Fetches all items of the provided technologies from the database.
     */
    private async fetchTechnologyItems(
        technologies: DatabaseTechnology[],
        options: Options,
    ): Promise<Record<string, Item>> {
        const itemKeys: string[] = [];
        for (const technology of technologies) {
            itemKeys.push(...technology.researchIngredientKeys);
        }

        return await this.itemRepository.findByKeys(itemKeys, options);
    }

    public async addUnlockingTechnologies(recipes: Recipe[], options: Options): Promise<RecipeWithTechnology[]> {
        const recipeKeys = recipes.map((recipe) => recipe.name);
        const technologies = await this.table.where("unlockedRecipes").anyOf(recipeKeys).toArray();
        const mappedTechnologies = await this.mapArray(technologies, options);

        const unlocks: Record<string, Technology> = {};
        for (const technology of Object.values(mappedTechnologies)) {
            for (const recipeKey of technology.unlockedRecipes) {
                unlocks["crafting." + recipeKey] = technology;
            }
        }

        return recipes.map((recipe) => {
            return {
                ...recipe,
                technology: unlocks[recipe.key] ?? null,
            };
        });
    }
}

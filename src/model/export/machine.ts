export type ExportMachineV1 = {
    kind: "machine.v1";
    type: string;
    name: string;
    labels: Record<string, string>;
    descriptions: Record<string, string>;
    icon: string;
    craftingCategories: string[];
    resourceCategories: string[];
    speed: number;
    itemSlots: number;
    fluidInputSlots: number;
    fluidOutputSlots: number;
    moduleSlots: number;
    energyUsage: number;
    energyUsageUnit: string;
};

const defaultMachineV1: Partial<ExportMachineV1> = {
    labels: {},
    descriptions: {},
    icon: "",
    craftingCategories: [],
    resourceCategories: [],
    speed: 1,
    itemSlots: 255,
    fluidInputSlots: 0,
    fluidOutputSlots: 0,
    moduleSlots: 0,
    energyUsage: 0,
    energyUsageUnit: "",
};

export const parseMachineV1 = (data: object): ExportMachineV1 => {
    return {
        ...defaultMachineV1,
        ...data,
    } as ExportMachineV1;
};

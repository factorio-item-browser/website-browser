export type ExportRecipeV1 = {
    kind: "recipe.v1";
    type: string;
    name: string;
    labels: Record<string, string>;
    descriptions: Record<string, string>;
    icon: string;
    time: number;
    category: string;
    ingredients: ExportRecipeV1Ingredient[];
    products: ExportRecipeV1Product[];
    enabled: boolean;
    hidden: boolean;
};

export type ExportRecipeV1Ingredient = {
    type: string;
    name: string;
    amount: number;
};

export type ExportRecipeV1Product = {
    type: string;
    name: string;
    amountMin: number;
    amountMax: number;
    probability: number;
};

const defaultRecipeV1: Partial<ExportRecipeV1> = {
    labels: {},
    descriptions: {},
    icon: "",
    time: 0,
    category: "",
    ingredients: [],
    products: [],
    enabled: false,
    hidden: false,
};

const defaultRecipeV1Ingredient: Partial<ExportRecipeV1Ingredient> = {
    amount: 1,
};

const defaultRecipeV1Product: Partial<ExportRecipeV1Product> = {
    amountMin: 1,
    amountMax: 1,
    probability: 1,
};

export const parseRecipeV1 = (data: object): ExportRecipeV1 => {
    const recipe = {
        ...defaultRecipeV1,
        ...data,
    } as ExportRecipeV1;

    recipe.ingredients = recipe.ingredients.map((ingredient) => {
        return {
            ...defaultRecipeV1Ingredient,
            ...ingredient,
        };
    });

    recipe.products = recipe.products.map((product) => {
        return {
            ...defaultRecipeV1Product,
            ...product,
        };
    });

    return recipe;
};

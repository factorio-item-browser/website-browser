export type ExportModV1 = {
    kind: "mod.v1";
    type: string;
    name: string;
    labels: Record<string, string>;
    descriptions: Record<string, string>;
    icon: string;
    author: string;
    version: string;
};

const defaultModV1: Partial<ExportModV1> = {
    labels: {},
    descriptions: {},
    icon: "",
    author: "",
    version: "",
};

export const parseModV1 = (data: object): ExportModV1 => {
    return {
        ...defaultModV1,
        ...data,
    } as ExportModV1;
};

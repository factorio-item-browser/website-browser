import { Buffer } from "buffer";
import { ExportItemV1, parseItemV1 } from "./item.ts";
import { ExportMachineV1, parseMachineV1 } from "./machine.ts";
import { ExportModV1, parseModV1 } from "./mod.ts";
import { ExportRecipeV1, parseRecipeV1 } from "./recipe.ts";
import { ExportTechnologyV1, parseTechnologyV1 } from "./technology.ts";

export type ExportData = ExportItemV1 | ExportMachineV1 | ExportModV1 | ExportRecipeV1 | ExportTechnologyV1;
export type DataCallback = (data: ExportData) => void | Promise<void>;

type Parser = (data: object) => ExportData;

export class DataReader {
    private parsers: Record<string, Parser> = {
        "item.v1": parseItemV1,
        "machine.v1": parseMachineV1,
        "mod.v1": parseModV1,
        "recipe.v1": parseRecipeV1,
        "technology.v1": parseTechnologyV1,
    };

    // Callback for further processing of the parsed data.
    public onData: DataCallback = () => {};

    public constructor(private readonly reader: ReadableStream<Uint8Array>) {}

    public async read(): Promise<void> {
        const reader = this.reader.getReader();

        let buffer = "";
        while (true) {
            const { done, value } = await reader.read();

            if (value) {
                buffer += Buffer.from(value).toString();

                const lines = buffer.split("\n");
                buffer = lines.pop() || "";

                for (const line of lines) {
                    const data = this.processLine(line);
                    if (data) {
                        this.onData(data);
                    }
                }
            }

            if (done) {
                break;
            }
        }
    }

    // Processes the provided line, parsing it to export data, if possible.
    private processLine(line: string): ExportData | null {
        const data = JSON.parse(line) as { kind: string };
        if (this.parsers[data.kind]) {
            return this.parsers[data.kind](data);
        }

        return null;
    }
}

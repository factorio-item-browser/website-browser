export type ExportTechnologyV1 = {
    kind: "technology.v1";
    type: string;
    name: string;
    labels: Record<string, string>;
    descriptions: Record<string, string>;
    icon: string;
    prerequisites: string[];
    researchIngredients: ExportTechnologyV1Ingredient[];
    researchCount: number;
    researchCountFormula: string;
    researchTime: number;
    unlockedRecipes: string[];
    level: number;
    maxLevel: number;
};

export type ExportTechnologyV1Ingredient = {
    type: string;
    name: string;
    amount: number;
};

const defaultTechnologyV1: Partial<ExportTechnologyV1> = {
    labels: {},
    descriptions: {},
    icon: "",
    prerequisites: [],
    researchIngredients: [],
    researchCount: 0,
    researchCountFormula: "",
    researchTime: 0,
    unlockedRecipes: [],
    level: 1,
    maxLevel: 1,
};

const defaultTechnologyV1Ingredient: Partial<ExportTechnologyV1Ingredient> = {
    amount: 1,
};

export const parseTechnologyV1 = (data: object): ExportTechnologyV1 => {
    const technology = {
        ...defaultTechnologyV1,
        ...data,
    } as ExportTechnologyV1;

    technology.researchIngredients = technology.researchIngredients.map((ingredient) => {
        return {
            ...defaultTechnologyV1Ingredient,
            ...ingredient,
        };
    });

    return technology;
};

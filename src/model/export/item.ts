export type ExportItemV1 = {
    kind: "item.v1";
    type: string;
    name: string;
    labels: Record<string, string>;
    descriptions: Record<string, string>;
    icon: string;
    stackSize: number;
    hidden: boolean;
};

export const parseItemV1 = (data: object): ExportItemV1 => {
    let item = {
        labels: {},
        descriptions: {},
        icon: "",
        hidden: false,
        ...data,
    } as Partial<ExportItemV1>;

    if (item.type === "item") {
        item = {
            stackSize: 50,
            ...item,
        };
    }

    return item as ExportItemV1;
};

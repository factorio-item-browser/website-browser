import { Provider } from "jotai";
import { RouterProvider } from "react-router-dom";
import { router } from "./router.tsx";
import { store } from "./state/store.ts";

function App() {
    return (
        <Provider store={store}>
            <RouterProvider router={router} />
        </Provider>
    );
}

export default App;

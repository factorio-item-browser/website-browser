import { MutableRefObject, useCallback, useEffect, useRef, useState } from "react";
import { useLocation } from "react-router-dom";

/**
 * Creates a paginated list from the provided list of items.
 * This function returns three values: The paginated list (up to the displayed page), whether there are still pages
 * left to display, and the callback to use to add the next page.
 */
export function usePagination<T>(items: T[], itemsPerPage: number): [T[], boolean, () => void] {
    const location = useLocation();
    const [page, setPage] = useState(1);

    useEffect(() => {
        setPage(1);
    }, [location.pathname]);

    const nextPage = useCallback(() => {
        setPage(page + 1);
    }, [page]);

    return [items.slice(0, page * itemsPerPage), page * itemsPerPage < items.length, nextPage];
}

/**
 * Creates an observer, triggering the provided callback as soon as it comes into view. The returned ref must be added
 * to the element to be observed.
 */
export function useScrollObserver(callback: () => void, options?: IntersectionObserverInit): MutableRefObject<null> {
    const observerTarget = useRef(null);
    useEffect(() => {
        const observer = new IntersectionObserver((items) => {
            if (items[0]?.isIntersecting) {
                callback();
            }
        }, options);

        const current = observerTarget.current;
        current && observer.observe(current);

        return () => {
            current && observer.unobserve(current);
        };
    }, [callback, options, observerTarget]);

    return observerTarget;
}

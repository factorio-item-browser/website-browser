import { RefObject, useLayoutEffect, useState } from "react";

/**
 * Creates a resize observer on the provided reference, and reports the current width of that element.
 * The returned value is the actual width of the referenced element if it currently overflows its parent, and 0 if not.
 */
export function useOverflowWidth(ref: RefObject<HTMLDivElement>): number {
    const [width, setWidth] = useState(0);

    useLayoutEffect(() => {
        let observerRef = null;
        const observer = new ResizeObserver((element) => {
            const target = element[0].target as HTMLDivElement;
            setWidth(target.offsetWidth < target.scrollWidth ? target.scrollWidth : 0);
        });

        if (ref.current) {
            observer.observe(ref.current);
            observerRef = ref.current;
        }

        return () => {
            if (observerRef) {
                observer.unobserve(observerRef);
            }
        };
    }, [ref]);

    return width;
}

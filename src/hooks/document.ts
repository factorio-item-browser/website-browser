import { useEffect } from "react";
import { useTranslation } from "react-i18next";

/**
 * Uses the default document title for the current page.
 */
export function useDefaultDocumentTitle(): void {
    const { t } = useTranslation("common");

    useEffect(() => {
        document.title = t("common:title.default");
    });
}

/**
 * Uses an extended title for the current page, inserting the provided title.
 */
export function useExtendedDocumentTitle(title: string): void {
    const { t } = useTranslation("common");

    useEffect(() => {
        document.title = t("common:title.extended", { title: title });
    }, [t, title]);
}

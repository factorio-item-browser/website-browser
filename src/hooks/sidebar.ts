import { useSetAtom } from "jotai";
import { useEffect } from "react";
import { Entity } from "../model/entity/entity.ts";
import { addViewedEntityAtom } from "../state/sidebar.ts";

// The hook for adding an entity to the list of viewed ones.
export function useViewedEntity(entity: Entity): void {
    const addViewedEntity = useSetAtom(addViewedEntityAtom);

    /* eslint-disable react-hooks/exhaustive-deps */
    useEffect(() => {
        (async () => {
            await addViewedEntity(entity);
        })();
    }, [entity.key]);
}

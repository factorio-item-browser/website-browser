export enum Theme {
    /** The system or browser is providing dictating what theme to use. */
    System = "system",
    /** The light theme is used always. */
    Light = "light",
    /** The dark theme is used always. */
    Dark = "dark",
}

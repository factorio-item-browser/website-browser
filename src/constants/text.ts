/** The styles available for the headlines. */
export enum HeadlineStyle {
    Default = "default",
    Sidebar = "sidebar",
}

/** The sizes available for the text. */
export enum TextSize {
    Small = "small",
    Medium = "medium",
    Large = "large",
    Headline = "headline",
    HeadlineSidebar = "headline-sidebar",
    Title = "title",
}

/** The styles available for the text. */
export enum TextStyle {
    Default = "default",
    Bold = "bold",
    Monospace = "monospace",
    Uppercase = "uppercase",
    White = "white",
}

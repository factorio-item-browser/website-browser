/** The breakpoints available for the page. */
export enum Breakpoint {
    /** Single-column layout with hidden sidebar. */
    Small = "small",
    /** Double column layout with hidden sidebar. */
    Medium = "medium",
    /** Double column layout with visible sidebar. */
    Large = "large",
    /** Triple column layout with visible sidebar. */
    Huge = "huge",
}

/** The sizes available for the icons. */
export enum IconSize {
    Tiny = "tiny",
    Small = "small",
    Medium = "medium",
    Large = "large",
    Huge = "huge",
}

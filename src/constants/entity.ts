/** The available types of items. */
export enum ItemType {
    Fluid = "fluid",
    Item = "item",
    Resource = "resource",
}

/** The available types of mods. */
export enum ModType {
    Mod = "mod",
}

/** The available types of machines. */
export enum MachineType {
    Machine = "machine",
}

/** The available types of recipes. */
export enum RecipeType {
    Crafting = "crafting",
    Mining = "mining",
    RocketLaunch = "rocket-launch",
}

/** The available types of technologies. */
export enum TechnologyType {
    Technology = "technology",
}

/** The available types of the entities in general. */
export type EntityType = ItemType | MachineType | ModType | RecipeType | TechnologyType;

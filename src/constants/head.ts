/** The styles available for the head component. */
export enum HeadStyle {
    Default = "default",
    Title = "title",
}

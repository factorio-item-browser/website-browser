/** The styles available for the section component. */
export enum SectionStyle {
    Default = "default",
    Title = "title",
    Sidebar = "sidebar",
}

import common from "./common.json";
import components from "./components.json";
import errors from "./errors.json";
import pages from "./pages.json";

export default {
    common: common,
    components: components,
    errors: errors,
    pages: pages,
};
